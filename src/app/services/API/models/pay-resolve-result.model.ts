export class PayResolveResult {
    payUrl?: string | null;
    static fromJS(data: any): PayResolveResult {
        data = typeof data === 'object' ? data : {};
        const result = new PayResolveResult();
        result.init(data);
        return result;
    }
    constructor(data?: PayResolveResult) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (<any>this)[property] = (<any>data)[property];
                }
            }
        }
    }
    init(data?: any) {
        if (data) {
            this.payUrl = data['payUrl'] !== undefined ? data['payUrl'] : <any>null;
        }
    }
    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data['payUrl'] = this.payUrl !== undefined ? this.payUrl : <any>null;
        return data;
    }
}
