export class CurrencyRates {
    usd!: number;
    eur!: number;

    static fromJS(data: any): CurrencyRates {
        data = typeof data === 'object' ? data : {};
        const result = new CurrencyRates();
        result.init(data);
        return result;
    }
    constructor(data?: CurrencyRates) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (<any>this)[property] = (<any>data)[property];
                }
            }
        }
    }
    init(data?: any) {
        if (data) {
            this.usd = data['usd'] !== undefined ? data['usd'] : <any>null;
            this.eur = data['eur'] !== undefined ? data['eur'] : <any>null;
        }
    }
    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data['usd'] = this.usd !== undefined ? this.usd : <any>null;
        data['eur'] = this.eur !== undefined ? this.eur : <any>null;
        return data;
    }
}
