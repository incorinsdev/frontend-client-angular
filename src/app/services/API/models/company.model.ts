export class Company {
    title!: string;
    inn!: string;
    kpp!: string;
    email!: string;
    phone!: string;

    constructor(data?: Company) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (<any>this)[property] = (<any>data)[property];
                }
            }
        }
    }

    static fromJS(data: any): Company {
        data = typeof data === 'object' ? data : {};
        const result = new Company();
        result.init(data);
        return result;
    }

    init(data?: any) {
        if (data) {
            this.title = data['title'] !== undefined ? data['title'] : <any>null;
            this.inn = data['inn'] !== undefined ? data['inn'] : <any>null;
            this.kpp = data['kpp'] !== undefined ? data['kpp'] : <any>null;
            this.email = data['email'] !== undefined ? data['email'] : <any>null;
            this.phone = data['phone'] !== undefined ? data['phone'] : <any>null;
        }
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data['title'] = this.title !== undefined ? this.title : <any>null;
        data['inn'] = this.inn !== undefined ? this.inn : <any>null;
        data['kpp'] = this.kpp !== undefined ? this.kpp : <any>null;
        data['email'] = this.email !== undefined ? this.email : <any>null;
        data['phone'] = this.phone !== undefined ? this.phone : <any>null;
        return data;
    }
}
