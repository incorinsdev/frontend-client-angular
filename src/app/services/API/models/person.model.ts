export class Person {
    firstName!: string;
    middleName?: string | null;
    surname!: string;
    birthDate!: Date | String;
    email!: string;
    phone!: string;

    constructor(data?: Person) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (<any>this)[property] = (<any>data)[property];
                }
            }
        }
    }

    static fromJS(data: any): Person {
        data = typeof data === 'object' ? data : {};
        const result = new Person();
        result.init(data);
        return result;
    }

    init(data?: any) {
        if (data) {
            this.firstName = data['firstName'] !== undefined ? data['firstName'] : <any>null;
            this.middleName = data['middleName'] !== undefined ? data['middleName'] : <any>null;
            this.surname = data['surname'] !== undefined ? data['surname'] : <any>null;
            this.birthDate = data['birthDate'] ? new Date(data['birthDate'].toString()) : <any>null;
            this.email = data['email'] !== undefined ? data['email'] : <any>null;
            this.phone = data['phone'] !== undefined ? data['phone'] : <any>null;
        }
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data['firstName'] = this.firstName !== undefined ? this.firstName : <any>null;
        data['middleName'] = this.middleName !== undefined ? this.middleName : <any>null;
        data['surname'] = this.surname !== undefined ? this.surname : <any>null;
        data['birthDate'] = this.birthDate
            ? typeof this.birthDate === 'string'
                ? this.birthDate
                : (this.birthDate as Date).toISOString()
            : <any>null;
        data['email'] = this.email !== undefined ? this.email : <any>null;
        data['phone'] = this.phone !== undefined ? this.phone : <any>null;
        return data;
    }
}
