export class Promo {
    value: string;
    ratio?: number;
    isValid?: boolean;

    constructor(data?: Promo) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    this[property] = data[property];
                }
            }
        }
    }

    static fromJS(data: any): Promo {
        data = typeof data === 'object' ? data : {};
        const result = new Promo();
        result.init(data);
        return result;
    }

    init(data?: Promo): void {
        if (data) {
            this.value = data['value'] ? data['value'] : null;
            this.ratio = data['ratio'] ? data['ratio'] : null;
            this.isValid = data['ratio'] ? true : false;
        }
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data['value'] = this.value !== undefined ? this.value : null;
        data['ratio'] = this.ratio !== undefined ? this.ratio : null;
        data['isValid'] = this.isValid !== undefined ? this.isValid : null;
        return data;
    }
}
