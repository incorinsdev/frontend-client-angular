export class City {
    id!: number;
    name: string;
    shops?: Shop[] | null;
    static fromJS(data: any): City {
        data = typeof data === 'object' ? data : {};
        const result = new City();
        result.init(data);
        return result;
    }
    constructor(data?: City) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (<any>this)[property] = (<any>data)[property];
                }
            }
        }
    }
    init(data?: any) {
        if (data) {
            this.id = data['id'] !== undefined ? data['id'] : <any>null;
            this.name = data['name'] !== undefined ? data['name'] : <any>null;
            if (data['shops'] && data['shops'].constructor === Array) {
                this.shops = [];
                for (const item of data['shops']) {
                    this.shops.push(Shop.fromJS(item));
                }
            }
        }
    }
    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data['id'] = this.id !== undefined ? this.id : <any>null;
        data['name'] = this.name !== undefined ? this.name : <any>null;
        if (this.shops && this.shops.constructor === Array) {
            data['shops'] = [];
            for (const item of this.shops) {
                data['shops'].push(item.toJSON());
            }
        }
        return data;
    }
}

export class Shop {
    id!: number;
    cityId!: number;
    picUrl!: string;
    title!: string;
    siteUrl!: string;
    address!: string;

    static fromJS(data: any): Shop {
        data = typeof data === 'object' ? data : {};
        const result = new Shop();
        result.init(data);
        return result;
    }
    constructor(data?: Shop) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (<any>this)[property] = (<any>data)[property];
                }
            }
        }
    }
    init(data?: any) {
        if (data) {
            this.id = data['id'] !== undefined ? data['id'] : <any>null;
            this.picUrl = data['picUrl'] !== undefined ? data['picUrl'] : <any>null;
            this.title = data['title'] !== undefined ? data['title'] : <any>null;
            this.cityId = data['cityId'] !== undefined ? data['cityId'] : <any>null;
            this.siteUrl = data['siteUrl'] !== undefined ? data['siteUrl'] : <any>null;
            this.address = data['address'] !== undefined ? data['address'] : <any>null;
        }
    }
    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data['id'] = this.id !== undefined ? this.id : <any>null;
        data['picUrl'] = this.picUrl !== undefined ? this.picUrl : <any>null;
        data['title'] = this.title !== undefined ? this.title : <any>null;
        data['cityId'] = this.cityId !== undefined ? this.cityId : <any>null;
        data['siteUrl'] = this.siteUrl !== undefined ? this.siteUrl : <any>null;
        data['address'] = this.address !== undefined ? this.address : <any>null;
        return data;
    }
}
