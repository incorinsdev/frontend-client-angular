export class Copter {
    serialNumber!: string;
    manufacturedYear!: number;
    modelTitle!: string;
    brandId!: number;

    constructor(data?: Copter) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (<any>this)[property] = (<any>data)[property];
                }
            }
        }
    }

    static fromJS(data: any): Copter {
        data = typeof data === 'object' ? data : {};
        const result = new Copter();
        result.init(data);
        return result;
    }

    init(data?: any) {
        if (data) {
            this.serialNumber = data['serialNumber'] !== undefined ? data['serialNumber'] : <any>null;
            this.manufacturedYear = data['manufacturedYear'] !== undefined ? data['manufacturedYear'] : <any>null;
            this.modelTitle = data['modelTitle'] !== undefined ? data['modelTitle'] : <any>null;
            this.brandId = data['brandId'] !== undefined ? data['brandId'] : <any>null;
        }
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data['serialNumber'] = this.serialNumber !== undefined ? this.serialNumber : <any>null;
        data['manufacturedYear'] = this.manufacturedYear !== undefined ? this.manufacturedYear : <any>null;
        data['modelTitle'] = this.modelTitle !== undefined ? this.modelTitle : <any>null;
        data['brandId'] = this.brandId !== undefined ? this.brandId : <any>null;
        return data;
    }
}
