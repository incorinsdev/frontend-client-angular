import { Person } from './person.model';
import { Company } from './company.model';
import { Policy } from './policy.model';
import { Promo } from './promo.model';

export class Order {
    policy!: Policy | null;
    personPayer?: Person | null;
    companyPayer?: Company | null;
    promoCode?: string | null;

    constructor(data?: Order) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (<any>this)[property] = (<any>data)[property];
                }
            }
        }
    }

    static fromJS(data: any): Order {
        data = typeof data === 'object' ? data : {};
        const result = new Order();
        result.init(data);
        return result;
    }

    init(data?: any) {
        if (data) {
            this.policy = data['policy'] ? Policy.fromJS(data['policy']) : <any>null;
            this.personPayer = data['personPayer'] ? Person.fromJS(data['personPayer']) : <any>null;
            this.companyPayer = data['companyPayer'] ? Company.fromJS(data['companyPayer']) : <any>null;
            this.promoCode = data['promoCode'] ? data['promoCode'] : null;
        }
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data['policy'] = this.policy ? this.policy.toJSON() : <any>null;
        data['personPayer'] = this.personPayer ? this.personPayer.toJSON() : <any>null;
        data['companyPayer'] = this.companyPayer ? this.companyPayer.toJSON() : <any>null;
        data['promoCode'] = this.promoCode ? this.promoCode : null;
        return data;
    }
}
