import { Copter } from './copter.model';

export class Policy {
    insuranceStartDate!: Date | String;
    limitId!: number;
    copter?: Copter | null;

    constructor(data?: Policy) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (<any>this)[property] = (<any>data)[property];
                }
            }
        }
    }

    static fromJS(data: any): Policy {
        data = typeof data === 'object' ? data : {};
        const result = new Policy();
        result.init(data);
        return result;
    }

    init(data?: any) {
        if (data) {
            this.insuranceStartDate = data['insuranceStartDate'] ? new Date(data['insuranceStartDate'].toString()) : <any>null;
            this.limitId = data['limitId'] !== undefined ? data['limitId'] : <any>null;
            this.copter = data['copter'] ? Copter.fromJS(data['copter']) : <any>null;
        }
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data['insuranceStartDate'] = this.insuranceStartDate
            ? typeof this.insuranceStartDate === 'string'
                ? this.insuranceStartDate
                : (this.insuranceStartDate as Date).toDateString()
            : <any>null;
        data['limitId'] = this.limitId !== undefined ? this.limitId : <any>null;
        data['copter'] = this.copter ? this.copter.toJSON() : <any>null;
        return data;
    }
}
