export class InsRequest {
    name!: string;
    city!: string;
    email!: string;
    insModelId!: number;

    static fromJS(data: any): InsRequest {
        data = typeof data === 'object' ? data : {};
        const result = new InsRequest();
        result.init(data);
        return result;
    }

    constructor(data?: InsRequest) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (<any>this)[property] = (<any>data)[property];
                }
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.name = data['name'] !== undefined ? data['name'] : <any>null;
            this.city = data['city'] !== undefined ? data['city'] : <any>null;
            this.email = data['email'] !== undefined ? data['email'] : <any>null;
            this.insModelId = data['insModelId'] !== undefined ? data['insModelId'] : <any>null;
        }
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data['name'] = this.name !== undefined ? this.name : <any>null;
        data['city'] = this.city !== undefined ? this.city : <any>null;
        data['email'] = this.email !== undefined ? this.email : <any>null;
        data['insModelId'] = this.insModelId !== undefined ? this.insModelId : <any>null;
        return data;
    }

    trick() { this.city = this.city + ' '; }
}
