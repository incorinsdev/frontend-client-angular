﻿import { mergeMap as _observableMergeMap, catchError as _observableCatch } from 'rxjs/operators';
import { Observable, throwError as _observableThrow, of as _observableOf } from 'rxjs';
import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpResponseBase } from '@angular/common/http';
import { Config } from '../web.config';
import { InsBrand } from './models/insBrand.model';
import { Limit } from './models/limit.model';
import { ResBrand } from './models/resBrand.model';
import { blobToText, throwException } from '../../GodModeControls/Shared/swagger.functions';
import { City } from './models/city-shop.model';
import { CurrencyRates } from './models/currencyRates.model';
import { Order } from './models/order.model';
import { PayResolveResult } from 'src/app/services/API/models/pay-resolve-result.model';
import { InsRequest } from './models/ins-request.model';
import { Promo } from './models/promo.model';

@Injectable()
export class APIClient {
    private baseUrl = Config.API_BASE_URL;
    protected jsonParseReviver: ((key: string, value: any) => any) | undefined = undefined;

    constructor(@Inject(HttpClient) private http: HttpClient) { }

    getInsBrands(): Observable<InsBrand[] | null> {
        let url_ = this.baseUrl + '/api/InsBrands';
        url_ = url_.replace(/[?&]$/, '');
        const options_: any = {
            observe: 'response',
            responseType: 'blob',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            })
        };
        return this.http.request('get', url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processGetInsBrands(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetInsBrands(<any>response_);
                } catch (e) {
                    return <Observable<InsBrand[] | null>><any>_observableThrow(e);
                }
            } else {
                return <Observable<InsBrand[] | null>><any>_observableThrow(response_);
            }
        }));
    }
    protected processGetInsBrands(response: HttpResponseBase): Observable<InsBrand[] | null> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        const _headers: any = {};
        if (response.headers) { for (const key of response.headers.keys()) { _headers[key] = response.headers.get(key); } }
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                const resultData200 = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
                if (resultData200 && resultData200.constructor === Array) {
                    result200 = [];
                    for (const item of resultData200) {
                        result200.push(InsBrand.fromJS(item));
                    }
                }
                return _observableOf(result200);
            }));
        } else if (status === 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return throwException('A server error occurred.', status, _responseText, _headers);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return throwException('An unexpected server error occurred.', status, _responseText, _headers);
            }));
        }
        return _observableOf<InsBrand[] | null>(<any>null);
    }

    getPromoCodeStatus(promoCode: string): Observable<Promo | null> {
        let url_ = this.baseUrl + '/api/PromoCode/{promoCode}';
        if (promoCode === undefined || promoCode === null) {
            throw new Error('The parameter \'promoCode\' must be defined.');
        }
        url_ = url_.replace('{promoCode}', encodeURIComponent('' + promoCode));
        url_ = url_.replace(/[?&]$/, '');

        const options_: any = {
            observe: 'response',
            responseType: 'blob',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            })
        };

        return this.http.request('get', url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processGetPromoCodeStatus(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetPromoCodeStatus(<any>response_);
                } catch (e) {
                    return <Observable<Promo>><any>_observableThrow(e);
                }
            } else {
                return <Observable<Promo>><any>_observableThrow(response_);
            }
        }));
    }

    protected processGetPromoCodeStatus(response: HttpResponseBase): Observable<Promo | null> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        const _headers: any = {};
        if (response.headers) { for (const key of response.headers.keys()) { _headers[key] = response.headers.get(key); } }
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                const resultData200 = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
                if (resultData200) {
                    result200 = Promo.fromJS(resultData200);
                }
                return _observableOf(result200);
            }));
        } else if (status === 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return throwException('A server error occurred.', status, _responseText, _headers);
            }));
        } else if (status === 400) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return _observableOf(Promo.fromJS({}));
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return throwException('An unexpected server error occurred.', status, _responseText, _headers);
            }));
        }
        return _observableOf<Promo | null>(<any>null);
    }

    getLimits(): Observable<Limit[] | null> {
        let url_ = this.baseUrl + '/api/Limits';
        url_ = url_.replace(/[?&]$/, '');
        const options_: any = {
            observe: 'response',
            responseType: 'blob',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            })
        };

        return this.http.request('get', url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processGetLimits(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetLimits(<any>response_);
                } catch (e) {
                    return <Observable<Limit[] | null>><any>_observableThrow(e);
                }
            } else {
                return <Observable<Limit[] | null>><any>_observableThrow(response_);
            }
        }));
    }
    protected processGetLimits(response: HttpResponseBase): Observable<Limit[] | null> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        const _headers: any = {};
        if (response.headers) { for (const key of response.headers.keys()) { _headers[key] = response.headers.get(key); } }
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                const resultData200 = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
                if (resultData200 && resultData200.constructor === Array) {
                    result200 = [];
                    for (const item of resultData200) {
                        result200.push(Limit.fromJS(item));
                    }
                }
                return _observableOf(result200);
            }));
        } else if (status === 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return throwException('A server error occurred.', status, _responseText, _headers);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return throwException('An unexpected server error occurred.', status, _responseText, _headers);
            }));
        }
        return _observableOf<Limit[] | null>(<any>null);
    }

    getCurrencyRates(): Observable<CurrencyRates | null> {
        let url_ = this.baseUrl + '/api/Currency';
        url_ = url_.replace(/[?&]$/, '');

        const options_: any = {
            observe: 'response',
            responseType: 'blob',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            })
        };

        return this.http.request('get', url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processCurrencyRates(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processCurrencyRates(<any>response_);
                } catch (e) {
                    return <Observable<CurrencyRates | null>><any>_observableThrow(e);
                }
            } else {
                return <Observable<CurrencyRates | null>><any>_observableThrow(response_);
            }
        }));
    }

    protected processCurrencyRates(response: HttpResponseBase): Observable<CurrencyRates | null> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        const _headers: any = {};
        if (response.headers) { for (const key of response.headers.keys()) { _headers[key] = response.headers.get(key); } }
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                const resultData200 = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
                result200 = resultData200 ? CurrencyRates.fromJS(resultData200) : <any>null;
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return throwException('An unexpected server error occurred.', status, _responseText, _headers);
            }));
        }
        return _observableOf<CurrencyRates | null>(<any>null);
    }

    getResBrands(): Observable<ResBrand[] | null> {
        let url_ = this.baseUrl + '/api/ResBrands';
        url_ = url_.replace(/[?&]$/, '');

        const options_: any = {
            observe: 'response',
            responseType: 'blob',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            })
        };

        return this.http.request('get', url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processGetResBrands(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetResBrands(<any>response_);
                } catch (e) {
                    return <Observable<ResBrand[] | null>><any>_observableThrow(e);
                }
            } else {
                return <Observable<ResBrand[] | null>><any>_observableThrow(response_);
            }
        }));
    }
    protected processGetResBrands(response: HttpResponseBase): Observable<ResBrand[] | null> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        const _headers: any = {};
        if (response.headers) { for (const key of response.headers.keys()) { _headers[key] = response.headers.get(key); } }
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                const resultData200 = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
                if (resultData200 && resultData200.constructor === Array) {
                    result200 = [];
                    for (const item of resultData200) {
                        result200.push(ResBrand.fromJS(item));
                    }
                }
                return _observableOf(result200);
            }));
        } else if (status === 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return throwException('A server error occurred.', status, _responseText, _headers);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return throwException('An unexpected server error occurred.', status, _responseText, _headers);
            }));
        }
        return _observableOf<ResBrand[] | null>(<any>null);
    }

    getRepairCities(): Observable<City[] | null> {
        let url_ = this.baseUrl + '/api/RepairCity';
        url_ = url_.replace(/[?&]$/, '');

        const options_: any = {
            observe: 'response',
            responseType: 'blob',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            })
        };

        return this.http.request('get', url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processCities(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processCities(<any>response_);
                } catch (e) {
                    return <Observable<City[] | null>><any>_observableThrow(e);
                }
            } else {
                return <Observable<City[] | null>><any>_observableThrow(response_);
            }
        }));
    }
    getSellerCities(): Observable<City[] | null> {
        let url_ = this.baseUrl + '/api/SellerCity';
        url_ = url_.replace(/[?&]$/, '');

        const options_: any = {
            observe: 'response',
            responseType: 'blob',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            })
        };

        return this.http.request('get', url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processCities(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processCities(<any>response_);
                } catch (e) {
                    return <Observable<City[] | null>><any>_observableThrow(e);
                }
            } else {
                return <Observable<City[] | null>><any>_observableThrow(response_);
            }
        }));
    }

    protected processCities(response: HttpResponseBase): Observable<City[] | null> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        const _headers: any = {};
        if (response.headers) { for (const key of response.headers.keys()) { _headers[key] = response.headers.get(key); } }
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                const resultData200 = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
                if (resultData200 && resultData200.constructor === Array) {
                    result200 = [];
                    for (const item of resultData200) {
                        result200.push(City.fromJS(item));
                    }
                }
                return _observableOf(result200);
            }));
        } else if (status === 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return throwException('A server error occurred.', status, _responseText, _headers);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return throwException('An unexpected server error occurred.', status, _responseText, _headers);
            }));
        }
        return _observableOf<City[] | null>(<any>null);
    }

    putOrder(order: Order | null): Observable<PayResolveResult | null> {
        let url_ = this.baseUrl + '/api/Order';
        url_ = url_.replace(/[?&]$/, '');

        const content_ = JSON.stringify(order);

        const options_: any = {
            body: content_,
            observe: 'response',
            responseType: 'blob',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            })
        };

        return this.http.request('post', url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processPutOrder(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processPutOrder(<any>response_);
                } catch (e) {
                    return <Observable<PayResolveResult | null>><any>_observableThrow(e);
                }
            } else {
                return <Observable<PayResolveResult | null>><any>_observableThrow(response_);
            }
        }));
    }

    protected processPutOrder(response: HttpResponseBase): Observable<PayResolveResult | null> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        const _headers: any = {};
        if (response.headers) { for (const key of response.headers.keys()) { _headers[key] = response.headers.get(key); } }
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                const resultData200 = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
                result200 = resultData200 ? PayResolveResult.fromJS(resultData200) : <any>null;
                return _observableOf(result200);
            }));
        } else if (status === 400) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return throwException('A server error occurred.', status, _responseText, _headers);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return throwException('An unexpected server error occurred.', status, _responseText, _headers);
            }));
        }
        return _observableOf<PayResolveResult | null>(<any>null);
    }

    sendMessage(contacts: InsRequest | null): Observable<void> {
        let url_ = this.baseUrl + '/api/Contacts';
        url_ = url_.replace(/[?&]$/, '');

        const content_ = JSON.stringify(contacts);

        const options_: any = {
            body: content_,
            observe: 'response',
            responseType: 'blob',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            })
        };

        return this.http.request('post', url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processSendMessage(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processSendMessage(<any>response_);
                } catch (e) {
                    return <Observable<void>><any>_observableThrow(e);
                }
            } else {
                return <Observable<void>><any>_observableThrow(response_);
            }
        }));
    }

    protected processSendMessage(response: HttpResponseBase): Observable<void> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        const _headers: any = {};
        if (response.headers) { for (const key of response.headers.keys()) { _headers[key] = response.headers.get(key); } }
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return _observableOf<void>(<any>null);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return throwException('An unexpected server error occurred.', status, _responseText, _headers);
            }));
        }
        return _observableOf<void>(<any>null);
    }
}
