import { Injectable } from '@angular/core';
import { ClientLimits } from '../models/client-limits.model';
import { APIClient } from './API/api.service';
import { ClientResBrands } from '../models/client-resBrands.model';
import { ClientInsBrands } from '../models/client-insBrands.model';
import { ClientCities } from '../models/client-city.model';
import { ClientPromo } from '../models/client-promo.model';
import { zip } from 'rxjs';
import { CurrencyRates } from './API/models/currencyRates.model';
import { Order } from './API/models/order.model';
import { Router } from '@angular/router';
import { InsRequest } from './API/models/ins-request.model';
import { mergeMap, catchError } from 'rxjs/operators';
import { throwError, of } from 'rxjs';

@Injectable()
export class CrossPageDataService {

    private _limits: ClientLimits = new ClientLimits();
    private _resBrands: ClientResBrands = new ClientResBrands();
    private _insBrands: ClientInsBrands = new ClientInsBrands();

    private _repairCities: ClientCities = new ClientCities();
    private _sellerCities: ClientCities = new ClientCities();
    private _promo: ClientPromo = new ClientPromo();

    private _openSidebar = false;

    public checkPromoCodeSent = false;
    public payRequestSent = false;

    public get promoCode(): ClientPromo {
        return this._promo;
    }

    public get limits(): ClientLimits {
        if (this._limits.isLoaded || this._limits.isLoading) {
            return this._limits;
        } else {
            this.loadLimits();
            return this._limits;
        }
    }
    public get resBrands(): ClientResBrands {
        if (this._resBrands.isLoaded || this._resBrands.isLoading) {
            return this._resBrands;
        } else {
            this.loadResBrands();
            return this._resBrands;
        }
    }
    public get insBrands(): ClientInsBrands {
        if (this._insBrands.isLoaded || this._insBrands.isLoading) {
            return this._insBrands;
        } else {
            this.loadInsBrands();
            return this._insBrands;
        }
    }

    public get repairCities(): ClientCities {
        if (this._repairCities.isLoaded || this._repairCities.isLoading) {
            return this._repairCities;
        } else {
            this.loadRepairCities();
            return this._repairCities;
        }
    }

    public get openSidebar (): boolean {
        return this._openSidebar;
    }

    public get sellerCities(): ClientCities {
        if (this._sellerCities.isLoaded || this._sellerCities.isLoading) {
            return this._sellerCities;
        } else {
            this.loadSellerCities();
            return this._sellerCities;
        }
    }

    public checkPromoCode(promoCode: string) {
        this.checkPromoCodeSent = true;
        return this.apiClient.getPromoCodeStatus(promoCode)
            .pipe(
                mergeMap(data => {
                    this.checkPromoCodeSent = false;
                    this._promo.init(data);
                    return of(data);
                })
            );
    }

    public sendPayRequest(order: Order) {
        if (!this.payRequestSent) {
            this.payRequestSent = true;
            this.apiClient.putOrder(order).subscribe(x => {
                this.payRequestSent = false;
                window.location.href = x.payUrl;
            }, error => {
                this.payRequestSent = false;
                this.redirectToServerErrorPage();
            });
        }
    }

    public sendInsRequest(request: InsRequest) {
        return this.apiClient.sendMessage(request).pipe(mergeMap(data => of(true))).pipe(catchError(error => throwError(false)));
    }

    constructor(private router: Router, private apiClient: APIClient) { }

    private loadLimits() {
        this._limits.isLoading = true;
        const multipleLoad = zip(
            this.apiClient.getLimits(),
            this.apiClient.getCurrencyRates()
        );
        multipleLoad.subscribe(dto => {
            if (dto !== null && dto instanceof Array && dto.length === 2) {
                dto.forEach(x => { if (x instanceof CurrencyRates) { this._limits.initRates(x); } else { this._limits.init(x); } });
                this._limits.isLoaded = true;
            } else { this.processRequestError(() => { this._limits.isLoading = false; }); }
        }, error => { this.processRequestError(() => { this._limits.isLoading = false; }); });
    }
    private loadResBrands() {
        this._resBrands.isLoading = true;
        this.apiClient.getResBrands().subscribe(
            data => { this._resBrands.init(data); },
            error => { this.processRequestError(() => { this._resBrands.isLoading = false; }); });
    }
    private loadInsBrands() {
        this._insBrands.isLoading = true;
        this.apiClient.getInsBrands().subscribe(
            data => {
                this._insBrands.init(data);
            },
            error => { this.processRequestError(() => { this._insBrands.isLoading = false; }); });
    }
    private loadRepairCities() {
        this._repairCities.isLoading = true;
        this.apiClient.getRepairCities().subscribe(
            data => {
                this._repairCities.init(data);
            },
            error => { this.processRequestError(() => { this._repairCities.isLoading = false; }); });
    }
    private loadSellerCities() {
        this._sellerCities.isLoading = true;
        this.apiClient.getSellerCities().subscribe(
            data => {
                this._sellerCities.init(data);
            },
            error => { this.processRequestError(() => { this._sellerCities.isLoading = false; }); });
    }



    private processRequestError(errorCallback: () => void) {
        this.redirectToServerErrorPage();
        setTimeout(() => {
            errorCallback();
        }, 5000);
    }


    public redirectToServerErrorPage() { }
    public redirectToNotFoundPage() { console.log('notFound'); }

    public toggleSidebar () {
        this._openSidebar = !this._openSidebar;
    }

}
