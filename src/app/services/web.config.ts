import { environment } from '../../environments/environment';

export class Config {
    public static API_BASE_URL = environment.apiUrl;
    // public static API_BASE_URL = 'http://172.16.50.139:5000';
}
