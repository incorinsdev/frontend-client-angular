import { Component, forwardRef } from '@angular/core';
import { GMCheckboxComponent } from '../../GodModeControls/Checkbox/checkbox.component';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-policy-checkbox',
  templateUrl: './policy-checkbox.component.html',
  styleUrls: ['./policy-checkbox.component.scss'],
  providers: [{ provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => PolicyCheckboxComponent), multi: true }]
})
export class PolicyCheckboxComponent extends GMCheckboxComponent {
  splash = '';
  pushToClick() {
    if (this.splash === '') {
      this.splash = 'splash';
      setTimeout(() => {
        this.splash = '';
      }, 800);
    }
  }
}
