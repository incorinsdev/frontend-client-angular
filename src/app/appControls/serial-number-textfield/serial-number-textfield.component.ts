import { Component, forwardRef } from '@angular/core';
import { GMTextFieldComponent } from '../../GodModeControls/TextField/text-field.component';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-serial-number-textfield',
  templateUrl: './serial-number-textfield.component.html',
  styleUrls: ['./serial-number-textfield.component.scss']
})
export class SerialNumberTextfieldComponent extends GMTextFieldComponent { }
