import { LoadedData } from '../GodModeControls/Shared/loaded-data.model';
import { ResBrand } from '../services/API/models/resBrand.model';
import { DropListValue } from '../GodModeControls/DropList/drop-list-value.model';

export class ClientResBrands extends LoadedData {
    brands: Array<DropListValue> = [];
    init(data: Array<ResBrand>) {
        const brnds = data;
        this.brands = brnds.map<DropListValue>(x => new DropListValue(x, x.name));
        this.isLoaded = true;
    }
}
