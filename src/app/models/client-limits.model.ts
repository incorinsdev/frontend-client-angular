import { LoadedData } from '../GodModeControls/Shared/loaded-data.model';
import { Limit } from '../services/API/models/limit.model';
import { CurrencyType } from '../services/API/models/currencyType.model';
import { CurrencyRates } from '../services/API/models/currencyRates.model';

export class ClientLimits extends LoadedData {
    limitsRUB: Array<Limit> = [];
    limitsEUR: Array<Limit> = [];
    limitsUSD: Array<Limit> = [];

    EURRate: number;
    USDRate: number;

    init(data: Array<Limit>) {
        this.limitsRUB = data.filter(x => x.currency === CurrencyType.Rub).sort((a, b) => a.awardLimit < b.awardLimit ? -1 : 1);
        this.limitsEUR = data.filter(x => x.currency === CurrencyType.Euro).sort((a, b) => a.awardLimit < b.awardLimit ? -1 : 1);
        this.limitsUSD = data.filter(x => x.currency === CurrencyType.Dollar).sort((a, b) => a.awardLimit < b.awardLimit ? -1 : 1);
        this.isLoaded = true;
    }

    initRates(val: CurrencyRates) {
        this.EURRate = val.eur;
        this.USDRate = val.usd;
    }

    getLimitById(id): Limit {
        const x1 = this.limitsRUB.find(x => x.id === id);
        const x2 = this.limitsEUR.find(x => x.id === id);
        const x3 = this.limitsUSD.find(x => x.id === id);
        return x1 ? x1 : (x2 ? x2 : x3);
    }

    getPrice(limit: Limit) {
        switch (limit.currency) {
            case CurrencyType.Rub:
                return limit.price;
            case CurrencyType.Euro:
                return Math.ceil(limit.price * this.EURRate / 10) * 10;
            case CurrencyType.Dollar:
                return Math.ceil(limit.price * this.USDRate / 10) * 10;
        }
    }
}
