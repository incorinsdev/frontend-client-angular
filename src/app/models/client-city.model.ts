import { LoadedData } from '../GodModeControls/Shared/loaded-data.model';
import { City } from '../services/API/models/city-shop.model';

export class ClientCities extends LoadedData {
    cities: Array<City> = [];
    init(data: Array<City>) {
        const cts = data;
        cts.forEach(city => {
            city.shops = city.shops.sort((a, b) => a.title < b.title ? 1 : -1);
        });
        this.cities = cts;
        this.isLoaded = true;
    }
}
