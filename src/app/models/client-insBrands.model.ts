import { LoadedData } from '../GodModeControls/Shared/loaded-data.model';
import { InsBrand } from '../services/API/models/insBrand.model';

export class ClientInsBrands extends LoadedData {
    brands: Array<InsBrand> = [];
    init(data: Array<InsBrand>) {
        const brnds = data;
        brnds.forEach(brand => {
            brand.brandModels = brand.brandModels.sort((a, b) => a.name < b.name ? 1 : -1);
            brand.brandModels.forEach(model => { model.brandName = brand.name; });
        });
        this.brands = brnds;
        this.isLoaded = true;
    }

    get models() { return this.brands.length ? this.brands.map(x => x.brandModels).reduce((prev, next) => prev.concat(next)) : []; }
}
