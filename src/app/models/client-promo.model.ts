import { LoadedData } from '../GodModeControls/Shared/loaded-data.model';
import { Promo } from '../services/API/models/promo.model';

export class ClientPromo extends LoadedData {
    ratio = 0;
    isValid = null;

    init(data: Promo) {
        this.ratio = data.ratio ? data.ratio : 0;
        this.isValid = data.ratio ? true : false;
        this.isLoaded = true;
    }

    clear () {
        this.ratio = 0;
    }
}
