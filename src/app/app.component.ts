import { Component } from '@angular/core';
import { CrossPageDataService } from './services/cross-page-data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(public cpdService: CrossPageDataService) {}
}
