import { GMModalWindowContent } from '../ModalWindow/modal-window-content.class';
import { Component, HostListener, ViewChildren, ElementRef, QueryList } from '@angular/core';
import { DropListValue } from './drop-list-value.model';
import { KeyCodes } from '../Shared/keycodes.enum';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gm-drop-list-window',
  templateUrl: './drop-list-window.component.html',
  styleUrls: ['./drop-list-window.component.scss']
})
export class GMDropListWindowComponent extends GMModalWindowContent {

  @ViewChildren('option') options: QueryList<ElementRef>;

  public values: Array<DropListValue> = [];

  private get selectedIndex() {
    let result = -1;
    if (this.values.length > 0) {
      let counter = -1;
      this.values.forEach(option => {
        counter++;
        if (option === this.window.closeResult) { result = counter; }
      });
    }
    return result;
  }

  @HostListener('document:keydown', ['$event']) onKeyUpHandler(event: KeyboardEvent) {
    if (event.keyCode === KeyCodes.Up || event.keyCode === KeyCodes.Down) {
      let selIndex = this.selectedIndex;
      event.preventDefault();
      if (event.keyCode === KeyCodes.Up && selIndex > 0) {
        selIndex--;
      } else if (event.keyCode === KeyCodes.Down && selIndex < this.values.length - 1) {
        selIndex++;
      }
      this.window.closeResult = this.values[selIndex];
    } else if (event.keyCode === KeyCodes.Enter || event.keyCode === KeyCodes.Tab || event.keyCode === KeyCodes.Escape) {
      this.selectOption(this.window.closeResult);
    }
  }

  selectOption(selVal: DropListValue) {
    this.window.closeResult = selVal;
    this.window.close();
  }
}
