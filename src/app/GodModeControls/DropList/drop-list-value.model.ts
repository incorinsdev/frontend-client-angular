export class DropListValue {
    value: any;
    text: string;
    constructor(value?: any, text?: any) {
        if (value != null) { this.value = value; }
        if (text != null) { this.text = text; }
    }
}
