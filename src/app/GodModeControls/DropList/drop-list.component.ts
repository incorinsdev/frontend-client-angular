import { Component, Input, forwardRef, HostListener, ElementRef, AfterViewInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ValueAccessorFunctions } from '../Shared/value-accessor-funcs.class';
import { DropListValue } from './drop-list-value.model';
import { GMModelWindowService } from '../ModalWindow/modal.service';
import { GMModalWindowPosition } from '../ModalWindow/modal-window-position.model';
import { GMDropListWindowComponent } from './drop-list-window.component';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gm-drop-list',
  templateUrl: './drop-list.component.html',
  styleUrls: ['./drop-list.component.scss'],
  providers: [{ provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => GMDropListComponent), multi: true }]
})
export class GMDropListComponent implements ControlValueAccessor, AfterViewInit {


  @Input() tabInd = 1;
  @Input() fieldname = '';
  @Input() placeholder = '';

  private _dropListWindow: GMDropListWindowComponent;
  private _dropListWindowPosition: GMModalWindowPosition = new GMModalWindowPosition();

  private _valueList: Array<DropListValue> = [];

  @Input()
  get valueList(): Array<DropListValue> { return this._valueList; }
  set valueList(val: Array<DropListValue>) { if (val !== this._valueList) { this._valueList = val; } }

  public value: DropListValue;
  public _disabled = false;

  private onChangeCallback: (_: any) => void = ValueAccessorFunctions.noop;
  private onTouchedCallback: () => void = ValueAccessorFunctions.noop;

  @HostListener('window:resize', ['$event']) onWindowResizeHandler(event) {
    this.setWindowPosition();
  }
  @HostListener('window:scroll', ['$event']) onWindowScrollHandler(event) {
    this.setWindowPosition();
  }

  writeValue(obj: DropListValue): void { if (obj !== this.value) { this.value = obj; this.onChangeCallback(this.value); } }
  registerOnChange(fn: any): void { this.onChangeCallback = fn; }
  registerOnTouched(fn: any): void { this.onTouchedCallback = fn; }
  setDisabledState?(isDisabled: boolean): void { if (isDisabled !== this._disabled) { this._disabled = isDisabled; } }
  onFocus() {
    this.onTouchedCallback();
    if (this._dropListWindow) { this._dropListWindow.window.close(); }
    this.setWindowPosition();
    this._dropListWindow = this.windowService.openDropListWindow(this.valueList, this.value, this._dropListWindowPosition);
    this._dropListWindow.window.ngOnClose.subscribe(data => {
      this.writeValue(data);
      this._dropListWindow = null;
    });
  }

  setWindowPosition() {
    this._dropListWindowPosition.x = this.elRef.nativeElement.getBoundingClientRect().left;
    this._dropListWindowPosition.ytop = this.elRef.nativeElement.getBoundingClientRect().top;
    this._dropListWindowPosition.ybottom = this.elRef.nativeElement.getBoundingClientRect().bottom;
    this._dropListWindowPosition.width = this.elRef.nativeElement.getBoundingClientRect().width;
  }

  ngAfterViewInit(): void { this.setWindowPosition(); }

  constructor(public elRef: ElementRef, public windowService: GMModelWindowService) { }


}
