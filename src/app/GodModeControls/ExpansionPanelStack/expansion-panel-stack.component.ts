import { Component, OnInit, ContentChildren, QueryList, AfterContentInit } from '@angular/core';
import { GMExpansionPanelComponent } from '../ExpansionPanel/expansion-panel.component';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gm-expansion-panel-stack',
  templateUrl: './expansion-panel-stack.component.html',
  styleUrls: ['./expansion-panel-stack.component.scss']
})
export class GMExpansionPanelStackComponent implements OnInit, AfterContentInit {


  @ContentChildren(GMExpansionPanelComponent) expPanels: QueryList<GMExpansionPanelComponent>;

  constructor() { }

  ngOnInit() {

  }
  ngAfterContentInit(): void {
    this.expPanels.forEach(expPanel => {
      expPanel.ngWasOpened.subscribe(res => {
        this.expPanels.forEach(element => {
          if (element.state === 'opened' && element !== expPanel) {
            element.close();
          }
        });
      });
    });
  }

}
