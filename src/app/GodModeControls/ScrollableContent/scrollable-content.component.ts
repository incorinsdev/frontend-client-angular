import { Component, ViewChild, ElementRef, HostListener } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gm-scrollable-content',
  templateUrl: './scrollable-content.component.html',
  styleUrls: ['./scrollable-content.component.scss']
})
export class GMScrollableContentComponent {
  @ViewChild('scrollPanel') scrollPanel: ElementRef;
  @ViewChild('content') content: ElementRef;
  @ViewChild('scrollPart') scrollPart: ElementRef;

  private dragScroll = false;
  private initCursorYPosition = 0;
  private initScrollBlockPosition = 0;

  get showScroll() {
    if (this.scrollPanel && this.content) {
      return this.scrollPanel.nativeElement.offsetHeight < this.content.nativeElement.offsetHeight;
    } else { return false; }
  }
  get scrolHeight() {
    if (this.scrollPanel && this.content) {
      const pH = this.scrollPanel.nativeElement.offsetHeight;
      const cH = this.content.nativeElement.offsetHeight;
      return pH < cH ? (pH * pH / cH) + 'px' : '0';
    } else { return '0'; }
  }
  get scrollTop() {
    if (this.scrollPanel && this.content) {
      const initPos = this.scrollPanel.nativeElement.getBoundingClientRect().top;
      const yPos = this.content.nativeElement.getBoundingClientRect().top;
      const cHeight = this.content.nativeElement.getBoundingClientRect().height;
      const pHeight = this.scrollPanel.nativeElement.getBoundingClientRect().height;
      return ((initPos - yPos) * pHeight / cHeight);
    } else { return 0; }
  }

  @HostListener('window:mouseup') onMouseup() {
    this.dragScroll = false;
    this.initCursorYPosition = 0;
    this.initScrollBlockPosition = 0;
  }

  @HostListener('window:mousemove', ['$event']) onMouseMove(event: MouseEvent) {
    if (this.dragScroll) {
      const cHeight = this.content.nativeElement.getBoundingClientRect().height;
      const pHeight = this.scrollPanel.nativeElement.getBoundingClientRect().height;
      const difference = event.clientY - this.initCursorYPosition;
      this.scrollPart.nativeElement.scrollTop = (((this.initScrollBlockPosition + difference) * cHeight) / pHeight);
    }
  }

  startDrag(event: MouseEvent) {
    this.dragScroll = true;
    this.initCursorYPosition = event.clientY;
    this.initScrollBlockPosition = this.scrollTop;
  }
  scrlPnl() { const m = this.scrollTop; }
}
