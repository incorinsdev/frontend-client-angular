export class GMDateFunctions {
    static today() {
        const d = new Date(Date.now());
        d.setHours(0, 0, 0, 0);
        return d;
    }
    static tomorrow() {
        const d = new Date(Date.now());
        d.setHours(0, 0, 0, 0);
        d.setDate(d.getDate() + 1);
        return d;
    }
    static tomorrowInYearPlus(yearCount: number) {
        const d = new Date(Date.now());
        d.setHours(0, 0, 0, 0);
        d.setFullYear(d.getFullYear() + yearCount);
        return d;
    }
    static tomorrowInMonthPlus(monthCount: number) {
        const d = new Date(Date.now());
        d.setHours(0, 0, 0, 0);
        d.setDate(d.getDate() + 1);
        d.setMonth(d.getMonth() + monthCount);
        return d;
    }
    static datesAreEqual(date1: Date, date2: Date) {
        return (date1 != null && date2 != null && date1.getTime() === date2.getTime()) || (date1 == null && date2 == null);
    }
    static adultBirthDate() {
        const tday = this.today();
        tday.setFullYear(tday.getFullYear() - 18);
        return tday;
    }

}
