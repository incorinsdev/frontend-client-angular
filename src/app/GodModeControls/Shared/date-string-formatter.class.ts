export class GMDateStringFormatter {
    static convertStringToDate(date: string) {
        if (this.isValidDate(date)) {
            const bits = date.split('.').map(x => Number(x));
            return new Date(bits[2], bits[1] - 1, bits[0], 0, 0, 0, 0);
        }
        return null;
    }
    static convertDateToString(date: Date) {
        const dd = date.getDate() < 10 ? '0' + date.getDate() : '' + date.getDate();
        const mm = (date.getMonth() + 1) < 10 ? '0' + (date.getMonth() + 1) : '' + (date.getMonth() + 1);
        return dd + '.' + mm + '.' + date.getFullYear();
    }
    static convertDateToFormattedString(date: Date) {
        return GMDateStringFormatter.convertDateToString(date).replace(/\./g, '-');
    }
    static isValidDate(date: string) {
        const bits = date.split('.').map(x => Number(x));
        if (bits.length === 3) {
            const d = new Date(bits[2], bits[1] - 1, bits[0]);
            return d.getFullYear() > 999 &&
                d.getFullYear() < 10000 &&
                d.getFullYear() === bits[2] &&
                (d.getMonth() + 1) === bits[1] &&
                d.getDate() === bits[0];
        }
        return false;
    }
}
