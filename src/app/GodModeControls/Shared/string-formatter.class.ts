export class GMStringFormatter {
    static addSpacesBetweenRanks(value: number, digitsAfterComma: number) {
        let result = '';
        const ranks: Array<number> = [];
        let sValue = Math.floor(value);
        const cda = value - sValue;
        while (sValue > 0) {
            ranks.push(Math.floor(sValue % 1000));
            sValue = Math.floor(sValue / 1000);
        }
        if (ranks.length === 0) {
            result = '0';
        } else {
            result = result + ranks[ranks.length - 1];
            for (let i = ranks.length - 2; i > -1; i--) {
                if (ranks[i] > 99) {
                    result = result + ' ' + ranks[i];
                } else if (ranks[i] > 9) {
                    result = result + ' 0' + ranks[i];
                } else {
                    result = result + ' 00' + ranks[i];
                }
            }
        }
        if (digitsAfterComma > 0) {
            result = result + '.';
            if (cda > 0) { result = result + Math.round(cda * Math.pow(10, digitsAfterComma)); } else {
                for (let i = 0; i < digitsAfterComma; i++) { result = result + '0'; }
            }
        }
        return result;
    }
}
