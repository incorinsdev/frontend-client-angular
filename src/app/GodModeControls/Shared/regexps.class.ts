export class GMRegularExpressions {
    // tslint:disable-next-line:max-line-length
    static readonly EMAIL_VALIDATION = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    static readonly ONLY_LETTERS = new RegExp('[a-zA-ZА-Яа-я]');
    static readonly ONLY_DIGITS = new RegExp('[0-9]');
    static readonly ONLY_RUSSIAN_LETTERS = new RegExp('[А-Яа-я]');
}
