import { Output, EventEmitter } from '@angular/core';

export class LoadedData {
    isLoading = false;
    private _isLoaded = false;
    get isLoaded() {
        return this._isLoaded;
    }
    set isLoaded(v) {
        this._isLoaded = v;
        if (v) {
            this.isLoading = false;
            this.loadComplete.emit(v);
        }
    }
    @Output() loadComplete = new EventEmitter<boolean>();
}
