import { Component, Input, Inject, Output, EventEmitter, Optional, Self } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ControlValueAccessor, NgControl } from '@angular/forms';
import { mergeMap as _mergeMap, catchError as _catchError } from 'rxjs/operators';
import { ValueAccessorFunctions } from '../Shared/value-accessor-funcs.class';

class FileUploadResponse { filePath: string; }

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gm-image-uploader',
  templateUrl: './image-uploader.component.html',
  styleUrls: ['./image-uploader.component.scss']
})
export class GMImageUploaderComponent implements ControlValueAccessor {
  public _disabled = false;
  @Input() baseUrl = '';
  @Input() uploadLocalUrl = '';
  private get uploadUrl() { return (this.baseUrl + this.uploadLocalUrl).replace(/[?&]$/, ''); }

  @Input() value = '';

  @Output() ngOnError: EventEmitter<any> = new EventEmitter<any>();



  private onChangeCallback: (_: any) => void = ValueAccessorFunctions.noop;
  private onTouchedCallback: () => void = ValueAccessorFunctions.noop;

  constructor(@Inject(HttpClient) private http: HttpClient, @Optional() @Self() public controlDir: NgControl) {
    if (this.controlDir) { this.controlDir.valueAccessor = this; }
  }

  fileChange(event) { this.uploadImage(event.target.files[0]); }

  private uploadImage(selectedFile: File) {
    const content_ = new FormData();
    content_.append('file', selectedFile, selectedFile.name);
    const options_: any = {
      body: content_,
      observe: 'response',
      responseType: 'json',
      headers: new HttpHeaders({})
    };
    this.http.post(this.uploadUrl, content_, options_).pipe(_mergeMap((resp: HttpResponse<FileUploadResponse>) => {
      this.writeValue(this.baseUrl + resp.body.filePath);
      return resp.body.filePath;
    })).pipe(_catchError(error => {
      this.ngOnError.emit(error);
      return '';
    })).subscribe(x => { });
  }

  writeValue(obj: string): void {
    if (obj !== this.value) {
      this.value = obj;
      this.onChangeCallback(this.value);
    }
  }
  registerOnChange(fn: any): void { this.onChangeCallback = fn; }
  registerOnTouched(fn: any): void { this.onTouchedCallback = fn; }
  setDisabledState?(isDisabled: boolean): void {
    if (isDisabled !== this._disabled) {
      this._disabled = isDisabled;
    }
  }

}
