import { EventEmitter, Output, Component } from '@angular/core';
import { Router, RoutesRecognized } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gm-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class GMHeaderComponent {
  currentUrl = '';
  @Output() pageNavigated: EventEmitter<string> = new EventEmitter<string>();
  constructor(private router: Router) {
    this.router.events.pipe(filter(event => event instanceof RoutesRecognized)).subscribe((event: RoutesRecognized) => {
      this.currentUrl = event.url;
      this.pageNavigated.emit(this.currentUrl);
    });
  }
}
