import {
  Component,
  Input,
  ViewChild,
  ElementRef,
  OnInit,
  Optional,
  Self,
  ViewEncapsulation
} from '@angular/core';
import { ControlValueAccessor, NgControl } from '@angular/forms';
import { ValueAccessorFunctions } from '../Shared/value-accessor-funcs.class';
import { GMMaskFormatter } from './mask-formatter.class';

export class FieldError {
  constructor(public errorName: string, public errorMessage: string) { }
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gm-text-field',
  templateUrl: './text-field.component.html',
  styleUrls: ['../Styles/gm-styles.scss']
})
export class GMTextFieldComponent implements ControlValueAccessor, OnInit {
  @ViewChild('input') inputRef: ElementRef;
  public value: string;
  public maskFormatter: GMMaskFormatter = new GMMaskFormatter();
  public disabled = false;

  private _focused = false;

  get focused() { return this._focused; }
  set focused(val: boolean) {
    if (val !== this._focused) {
      this._focused = val;
      if (this.focused) {
        this.onTouchedCallback();
      } else { this.checkErrors(); }
    }
  }

  public hasError = false;
  public error = '';
  splash = '';

  @Input() tabInd = 1;
  @Input() label = '';
  @Input() placeholder = '';
  @Input() type = 'text';
  @Input() errors: Array<FieldError> = [];

  @Input()
  get mask() {
    return this.maskFormatter.mask;
  }
  set mask(val: string) {
    if (val !== this.maskFormatter.mask) {
      this.maskFormatter.mask = val;
    }
  }

  private onChangeCallback: (_: any) => void = ValueAccessorFunctions.noop;
  private onTouchedCallback: () => void = ValueAccessorFunctions.noop;

  constructor(@Optional() @Self() public controlDir: NgControl) {
    if (this.controlDir) {
      this.controlDir.valueAccessor = this;
    }
  }

  writeValue(obj: string): void {
    if (obj !== this.maskFormatter.maskedValue) {
      if (obj != null) {
        this.maskFormatter.maskedValue = obj;
      } else {
        this.maskFormatter.maskedValue = '';
      }
      this.value = this.maskFormatter.maskedValue;
      this.onChangeCallback(this.value);
      this.checkErrors();
    }
  }

  registerOnChange(fn: any): void { this.onChangeCallback = fn; }
  registerOnTouched(fn: any): void { this.onTouchedCallback = fn; }

  setDisabledState?(isDisabled: boolean): void {
    if (isDisabled !== this.disabled) { this.disabled = isDisabled; }
  }

  ngOnInit(): void {
    if (this.inputRef != null) {
      this.maskFormatter.input = this.inputRef;
    }
  }

  checkErrors() {
    if (this.controlDir) {
      this.hasError = this.controlDir.invalid;
      if (this.hasError && this.errors) {
        this.errors.forEach(err => {
          if (this.controlDir.hasError(err.errorName)) {
            this.error = err.errorMessage;
          }
        });
      }
    }
  }

  onControlClick() {
    if (!this.disabled) {
      this.focused = true;
    }
  }

  pushToCheckErrors() {
    this.checkErrors();
    if (this.hasError && this.splash === '') {
      this.splash = 'splash';
      setTimeout(() => {
        this.splash = '';
      }, 800);
    }
  }
}
