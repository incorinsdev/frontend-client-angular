import { GMRegularExpressions } from '../Shared/regexps.class';
import { ElementRef } from '@angular/core';

export class GMMaskFormatter {

    input: ElementRef;
    numberRegExp = GMRegularExpressions.ONLY_DIGITS;
    letterRegularExp = GMRegularExpressions.ONLY_LETTERS;
    numberSymbol = '_';
    letterSymbol = '?';

    private _mask = '';
    private _unformattedValue = '';
    private _oldMaskedValue = '';
    private _maskedValue = '';

    private _caretPosition = 0;
    private _minimalCaretPosition = 0;
    private _selectionStart = 0;
    private _selectionEnd = 0;

    valueWithRemainingMask = '';
    remainingMask = '';

    get mask() { return this._mask; }
    set mask(newMask: string) {
        this._mask = newMask;
        this._minimalCaretPosition = 0;
        this.valueWithRemainingMask = this._mask;
        this.remainingMask = this._mask;
        while (this._mask[this._minimalCaretPosition] === this.numberSymbol ||
            this._mask[this._minimalCaretPosition] === this.letterSymbol) {
            this._minimalCaretPosition++;
        }
        if (this._mask !== '') {
            if (this.input) {
                this.getCaretPosition();
            }
            this._unformattedValue = this._maskedValue;
            this._oldMaskedValue = this._maskedValue;
            this.defineChanges();
            if (this.input) {
                this.input.nativeElement.value = this._maskedValue;
                this.setCaretPosition();
            }
            this.recalculateMaskPlaceholder();
        }
    }

    get maskedValue(): string { return this._maskedValue; }
    set maskedValue(value: string) {
        if (value !== this._maskedValue) {
            if (this._mask !== '') {
                this.getCaretPosition();
                if (value != null) {
                    this._unformattedValue = value;
                } else {
                    this._unformattedValue = '';
                }
                this._oldMaskedValue = this._maskedValue;
                this.defineChanges();
                if (this.input != null) {
                    this.input.nativeElement.value = this._maskedValue;
                }
                this.setCaretPosition();
                this.recalculateMaskPlaceholder();
            } else {
                this._maskedValue = value;
                if (this.input != null) {
                    this.input.nativeElement.value = this._maskedValue;
                }
            }
        }
    }

    getSelection() {
        const selStart = this.input.nativeElement.selectionStart;
        const selEnd = this.input.nativeElement.selectionEnd;
        if (selStart <= selEnd) {
            this._selectionStart = selStart;
            this._selectionEnd = selEnd;
        } else {
            this._selectionStart = selEnd;
            this._selectionEnd = selStart;
        }
    }

    setNewCarretPosition(pos: number) {
        this.input.nativeElement.setSelectionRange(pos, pos, 'none');
    }

    private setCaretPosition() {
        this.input.nativeElement.setSelectionRange(this._caretPosition, this._caretPosition, 'none');
    }

    private getCaretPosition() {
        if (this.input.nativeElement.selectionStart === this.input.nativeElement.selectionEnd) {
            this._caretPosition = this.input.nativeElement.selectionStart;
        }
    }

    private defineChanges() {
        if (this._unformattedValue !== this._oldMaskedValue) { // есть изменения
            if (this._unformattedValue.length > this._oldMaskedValue.length) { // увеличение символов
                // проверка на вставку (курсор в конце изменений)
                const oldCaretPos = this._oldMaskedValue.length - (this._unformattedValue.length - this._caretPosition);
                const endPart = this._unformattedValue.substring(this._caretPosition, this._unformattedValue.length);
                const startPart = this._unformattedValue.substring(0, oldCaretPos);
                if (startPart + endPart === this._oldMaskedValue) { // произошло добавление
                    this._maskedValue = this.formatValueFromStartPart(startPart);
                } else { // произошла вставка с частичной заменой (была селекция) курсор в конце изменений
                    const nonSelectEndPart = this._oldMaskedValue.substring(this._selectionEnd, this._oldMaskedValue.length);
                    const nonSelectStartPart = this._oldMaskedValue.substring(0, this._selectionStart);
                    this._maskedValue = this.formatValueFromStartPart(nonSelectStartPart);
                }
            } else if (this._unformattedValue.length < this._oldMaskedValue.length) { // уменьшение символов
                // проверка на удаление (курсор в начале изменений)
                const oldCaretPos = this._oldMaskedValue.length - (this._unformattedValue.length - this._caretPosition);
                const endPart = this._oldMaskedValue.substring(oldCaretPos, this._oldMaskedValue.length);
                const startPart = this._oldMaskedValue.substring(0, this._caretPosition);
                if (startPart + endPart === this._unformattedValue) { // произошло удаление
                    this._maskedValue = this.formatValueFromStartPart(startPart);
                } else { // произошла удаление с частичной заменой (была селекция) курсор в конце изменений
                    const nonSelectEndPart = this._oldMaskedValue.substring(this._selectionEnd, this._oldMaskedValue.length);
                    const nonSelectStartPart = this._oldMaskedValue.substring(0, this._selectionStart);
                    this._maskedValue = this.formatValueFromStartPart(nonSelectStartPart);
                }
            } else { // произошла замена (была селекция) курсор в конце изменений
                const nonSelectEndPart = this._oldMaskedValue.substring(this._selectionEnd, this._oldMaskedValue.length);
                const nonSelectStartPart = this._oldMaskedValue.substring(0, this._selectionStart);
                this._maskedValue = this.formatValueFromStartPart(nonSelectStartPart);
            }
        }
    }
    private formatValueFromStartPart(startPart: string): string {
        let result = startPart;
        let counterV = startPart.length;
        let counterM = startPart.length;
        while (counterV < this._unformattedValue.length && counterM < this._mask.length) {
            const newSymb = this.formatMaskSymbol(this._unformattedValue[counterV], this._mask[counterM]);
            if (newSymb === this._unformattedValue[counterV]) {
                result = result + newSymb;
                counterV++;
                counterM++;
            } else if (newSymb === '') {
                counterV++;
            } else {
                result = result + newSymb;
                if (counterM < this._caretPosition) {
                    this._caretPosition++;
                }
                counterM++;
            }
        }
        return result;
    }
    private formatMaskSymbol(symbol: string, maskSymbol: string) {
        if ((maskSymbol === this.numberSymbol && this.numberRegExp.test(symbol)) ||
            (maskSymbol === this.letterSymbol && this.letterRegularExp.test(symbol))) {
            return symbol;
        } else if (maskSymbol === this.numberSymbol || maskSymbol === this.letterSymbol) {
            return '';
        } else {
            return maskSymbol;
        }
    }
    private recalculateMaskPlaceholder() {
        if (this._maskedValue != null) {
            let result = this._maskedValue;
            let counter = this._maskedValue.length;
            while (counter < this._mask.length) {
                result = result + this._mask[counter];
                counter++;
            }
            this.valueWithRemainingMask = result;
            this.remainingMask = this._mask.substring(this.maskedValue.length, this._mask.length);
        } else {
            this.valueWithRemainingMask = this._mask;
            this.remainingMask = this._mask;
        }
    }
}
