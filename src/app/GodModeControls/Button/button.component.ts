import { Component, Input, Output, EventEmitter, HostListener } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gm-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class GMButtonComponent {
  @Input() isBusy = false;
  @Input() icon = '';
  @Input() text = '';
  @Input() tabInd = 1;
  public focused = false;
  private _disabled = false;
  shakeClass = '';

  @Input() get disabled() { return this._disabled || this.isBusy; }
  set disabled(val: boolean) { if (val !== this._disabled) { this._disabled = val; } }

  @Output() action: EventEmitter<void> = new EventEmitter<void>();

  @HostListener('document:keyup.Enter', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    if (this.focused) { this.onAction(); }
  }
  onAction() {
    if (!this.disabled) {
      this.action.emit();
    }
  }
  shake() {
    if (this.shakeClass === '') {
      this.shakeClass = 'shake';
      setTimeout(() => {
        this.shakeClass = '';
      }, 820);
    }
  }
}
