import { Component, Input, Output, EventEmitter, } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gm-expansion-panel',
  templateUrl: './expansion-panel.component.html',
  styleUrls: ['./expansion-panel.component.scss'],
  animations: [
    trigger('collapseExpand', [
      state('opened', style({ height: '*', opacity: 1 })),
      state('closed', style({ height: 0, opacity: 0 })),
      transition('closed => opened', animate('400ms cubic-bezier(0.25, 0.8, 0.25, 1)')),
      transition('opened => closed', animate('400ms cubic-bezier(0.25, 0.8, 0.25, 1)')),
    ])
  ]
})
export class GMExpansionPanelComponent {
  @Input() state: 'opened' | 'closed' = 'closed';
  @Input() title = '';
  @Input() subTitle = '';
  @Output() ngWasOpened = new EventEmitter();

  get openText() {
    return this.state === 'opened' ? 'Закрыть' : 'Подробнее';
  }
  get openColor() {
    return this.state === 'opened' ? 'gray' : 'blue';
  }

  collapseExpand() {
    if (this.state === 'opened') {
      this.state = 'closed';
    } else {
      this.state = 'opened';
      this.ngWasOpened.emit();
    }
  }
  close() { this.state = 'closed'; }

}
