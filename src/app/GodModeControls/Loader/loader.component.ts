import { Component, Input } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gm-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss'],
  animations: [
    trigger('loaderAnimation', [
      state('show', style({ opacity: 1 })),
      state('hide', style({ opacity: 0 })),
      transition('show <=> hide', animate('300ms cubic-bezier(0.25, 0.8, 0.25, 1)'))
    ])

  ]
})
export class GMLoaderComponent {
  busyState = 'show';
  contentState = 'hide';
  showClass = 'busy';
  private _busy = true;
  @Input()
  get busy() { return this._busy; }
  set busy(val: boolean) {
    if (this._busy !== val) {
      this._busy = val;
      if (val) { this.contentState = 'hide'; } else { this.busyState = 'hide'; }
    }
  }
  onContentAnimationEnd() {
    if (this.contentState === 'hide') {
      this.busyState = 'show';
      this.showClass = 'busy';
    }
  }
  onBusyAnimationEnd() {
    if (this.busyState === 'hide') {
      this.contentState = 'show';
      this.showClass = 'content';
    }
  }
}
