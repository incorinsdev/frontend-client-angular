import { trigger, style, animate, state, transition } from '@angular/animations';

const ANIMATION_TIMINGS = '300ms cubic-bezier(0.25, 0.8, 0.25, 1)';
const ZERO_TIMINGS = '0ms';

export class GMSliderAnimations {
    static animationState: 'void' | 'slide' = 'void';
    static slideAnimation = trigger('carouselAnimation', [
        state('void', style({ marginLeft: '{{left}}' }), { params: { left: 0 } }),
        state('slide', style({ marginLeft: '{{left}}' }), { params: { left: 0 } }),
        transition('* => slide', animate(ANIMATION_TIMINGS)),
        transition('* => void', animate(ZERO_TIMINGS)),
    ]);
}
