import {
  EventEmitter,
  Input,
  Output,
  Component,
  ContentChildren,
  ViewChild,
  ElementRef,
  Renderer2,
  QueryList,
  AfterContentInit,
  Inject
} from '@angular/core';
import { GMCarouselItemComponent } from './CarouselItem/carousel-item.component';
import { GMSliderAnimations } from './slider.animations';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gm-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
  animations: [GMSliderAnimations.slideAnimation]
})
export class GMCarouselComponent implements AfterContentInit {

  isAnimating = false;
  _totalWidth = 0;
  _marginLeft = 0;

  get marginLeft() {
    return `-${this.getMarginWidth(this._currentPosition)}px`;
  }

  get totalWidth() {
    return `${this._totalWidth}px`;
  }

  _currentPosition = 0;

  @Input() get currentPosition() { return this._currentPosition; }
  set currentPosition(val: number) { if (val !== this._currentPosition) { this.slideToPosition(val); } }

  @ContentChildren(GMCarouselItemComponent) items: QueryList<GMCarouselItemComponent>;

  @Output() eventAnimationDone: EventEmitter<number> = new EventEmitter<number>();

  @ViewChild('carContainer') container: ElementRef;

  animationState = GMSliderAnimations.animationState;

  private getMarginWidth(index: number) {
    let totalWidth = 0;
    if (this.items) {
      let newInd = index > this.items.length - 1 ? this.items.length - 1 : index;
      this.items.forEach(item => {
        if (newInd > 0) { totalWidth = totalWidth + item.el.nativeElement.getBoundingClientRect().width; }
        newInd--;
      });
    }
    return totalWidth;
  }

  private getTotalWidth() {
    let totalWidth = 0;
    this.items.forEach(item => {
      totalWidth = totalWidth + item.el.nativeElement.getBoundingClientRect().width;
    });
    return totalWidth;
  }

  ngAfterContentInit(): void {
    this._totalWidth = this.getTotalWidth();
    this.setPosition(this._currentPosition);
  }

  animationDone() {
    if (this.isAnimating) {
      this.isAnimating = false;
      this.animationState = 'void';
      this.eventAnimationDone.emit(this._currentPosition);
    }
  }

  animationStart() {
    this.isAnimating = true;
  }

  constructor(@Inject(Renderer2) private renderer: Renderer2) { }

  setPosition(index: number) {
    this._currentPosition = index;
    this.animationState = 'void';
    const mL = -this.getMarginWidth(index) + 'px';
    this.renderer.setStyle(this.container.nativeElement, 'margin-left', mL + 'px');
  }

  slideToPosition(index: number) {
    if (!this.isAnimating && this._currentPosition !== index) {
      this._currentPosition = index;
      this.isAnimating = true;
      this.animationState = 'slide';
    }
  }
}
