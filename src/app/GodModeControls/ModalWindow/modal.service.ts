import { Injectable, Injector, ComponentFactoryResolver, ApplicationRef, EmbeddedViewRef, Type } from '@angular/core';
import { GMModalWindow } from './modal-window.component';
import { GMModalWindowContent } from './modal-window-content.class';
import { DropListValue } from '../DropList/drop-list-value.model';
import { GMDropListWindowComponent } from '../DropList/drop-list-window.component';
import { GMModalWindowPosition } from './modal-window-position.model';
import { GMCalendarPopupComponent } from '../DatePicker/CalendarPopup/calendar-popup.component';
import { GMDateFunctions } from '../Shared/date.functions';


@Injectable()
export class GMModelWindowService {
  constructor(private appRef: ApplicationRef, private injector: Injector, private componentFactoryResolver: ComponentFactoryResolver) { }

  openWindowsIds: Array<number> = [];

  openWindow(content: Type<GMModalWindowContent>, position?: GMModalWindowPosition): any {
    const newId = this.pushNewId();
    const windowRef = this.componentFactoryResolver.resolveComponentFactory(GMModalWindow).create(this.injector);

    this.appRef.attachView(windowRef.hostView);
    const domWindowElem = (windowRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
    domWindowElem.querySelector('.gm-modal-window').id = 'gm-window' + newId;
    document.body.appendChild(domWindowElem);
    windowRef.instance.windowId = 'gm-window' + newId;
    const contentRef = this.componentFactoryResolver.resolveComponentFactory(content).create(this.injector);
    this.appRef.attachView(contentRef.hostView);
    const domElem = (contentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
    document.getElementById('gm-window' + newId).appendChild(domElem);
    contentRef.instance.window = windowRef.instance;
    if (position) { contentRef.instance.window.position = position; }
    windowRef.instance.ngOnClose.subscribe(data => {
      this.appRef.detachView(contentRef.hostView);
      this.appRef.detachView(windowRef.hostView);
      contentRef.destroy();
      windowRef.destroy();
      this.openWindowsIds = this.openWindowsIds.filter(x => x !== newId);
    });
    return contentRef.instance;
  }

  private pushNewId(): number {
    let result = 0;
    while (this.openWindowsIds.indexOf(result) > -1) {
      result++;
    }
    this.openWindowsIds.push(result);
    return result;
  }

  openDropListWindow(valueArray: Array<DropListValue>, selectedValue: DropListValue, position: GMModalWindowPosition) {
    const DRwindow = this.openWindow(GMDropListWindowComponent, position);
    DRwindow.values = valueArray;
    DRwindow.window.closeResult = selectedValue;
    return DRwindow;
  }
  openDatePickerWindow(
    position: GMModalWindowPosition,
    selectedDate?: Date,
    startDate?: Date,
    maxDate?: Date,
    minDate?: Date,
    mode?: string,
    firstDayOfWeek?: number) {
    const DPwindow = this.openWindow(GMCalendarPopupComponent, position);
    DPwindow.firstDayOfWeek = firstDayOfWeek != null ? firstDayOfWeek : 1;
    DPwindow.startDate = startDate ? startDate : GMDateFunctions.today();
    DPwindow.date = selectedDate;
    DPwindow.maxDate = maxDate;
    DPwindow.minDate = minDate;
    DPwindow.selectedYear = DPwindow.startDate.getFullYear();
    DPwindow.selectedMonth = DPwindow.startDate.getMonth();
    DPwindow.mode = mode ? mode : 'dateSelection';
    return DPwindow;
  }

  getBackDropHeight() {
    return document.querySelector('.gm-modalBackdrop').scrollHeight;
  }
}
