import { Renderer2, Inject } from '@angular/core';
import { GMModalWindow } from './modal-window.component';

export class GMModalWindowContent {
  public window: GMModalWindow;
  constructor(@Inject(Renderer2) public renderer: Renderer2) { }
}
