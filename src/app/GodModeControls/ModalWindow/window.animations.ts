import { trigger, style, animate, state, transition } from '@angular/animations';

const ANIMATION_TIMINGS = '300ms cubic-bezier(0.25, 0.8, 0.25, 1)';

export class GMWindowAnimations {
    static animationState: 'dropOut' | 'dropIn' | 'fadeIn' | 'fadeOut' = 'dropIn';
    static heightAnimation = trigger('windowAnimation', [
        state('dropOut', style({ height: '*', opacity: 1 })),
        state('dropIn', style({ height: 0, opacity: 0 })),
        state('fadeIn', style({ height: '*', opacity: 1, transform: 'translate(-50%,-50%)' })),
        state('fadeOut', style({ height: '*', opacity: 0, transform: 'translate(-50%,-25%) scale(0.9)' })),
        transition('dropOut <=> dropIn', animate(ANIMATION_TIMINGS)),
        transition('fadeIn <=> fadeOut', animate(ANIMATION_TIMINGS)),
    ]);
}

