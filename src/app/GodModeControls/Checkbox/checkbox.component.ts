import { Component, Input, forwardRef, HostListener } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ValueAccessorFunctions } from '../Shared/value-accessor-funcs.class';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gm-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
  providers: [{ provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => GMCheckboxComponent), multi: true }]
})
export class GMCheckboxComponent implements ControlValueAccessor {

  @Input() tabInd = 1;
  @Input() isRadio = false;
  public value = false;
  public _disabled = false;
  public focused = false;

  private onChangeCallback: (_: any) => void = ValueAccessorFunctions.noop;
  private onTouchedCallback: () => void = ValueAccessorFunctions.noop;

  @HostListener('document:keydown.Enter') onKeyUpHandler() { if (this.focused) { this.checkOrUncheck(); } }
  writeValue(obj: boolean): void {
    if (obj !== this.value) {
      this.value = obj;
      this.onChangeCallback(this.value);
    }
  }
  registerOnChange(fn: any): void { this.onChangeCallback = fn; }
  registerOnTouched(fn: any): void { this.onTouchedCallback = fn; }
  setDisabledState?(isDisabled: boolean): void { if (isDisabled !== this._disabled) { this._disabled = isDisabled; } }
  onFocus() { this.focused = true; this.onTouchedCallback(); }
  onBlur() { this.focused = false; }
  checkOrUncheck() { if (!this.isRadio) { this.writeValue(!this.value); } else { this.writeValue(true); } }



}
