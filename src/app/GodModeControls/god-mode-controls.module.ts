import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GMButtonComponent } from './Button/button.component';
import { GMTextFieldComponent } from './TextField/text-field.component';
import { GMModelWindowService } from './ModalWindow/modal.service';
import { GMModalWindow } from './ModalWindow/modal-window.component';
import { GMDropListComponent } from './DropList/drop-list.component';
import { GMDropListWindowComponent } from './DropList/drop-list-window.component';
import { GMCheckboxComponent } from './Checkbox/checkbox.component';
import { GMDatePikerComponent } from './DatePicker/date-picker.component';
import { GMDateCellComponent } from './DatePicker/date-cell.component';
import { GMCalendarPopupComponent } from './DatePicker/CalendarPopup/calendar-popup.component';
import { GMCarouselComponent } from './Carousel/carousel.component';
import { GMCarouselItemComponent } from './Carousel/CarouselItem/carousel-item.component';
import { GMCalendarDaysPartComponent } from './DatePicker/CalendarPopup/DaysPart/days-part.component';
import { GMCalendarYearPartComponent } from './DatePicker/CalendarPopup/YearPart/year-part.component';
import { GMCalendarMonthPartComponent } from './DatePicker/CalendarPopup/MonthPart/month-part.component';
import { GMHeaderComponent } from './Header/header.component';
import { GMLoaderComponent } from './Loader/loader.component';
import { GMExpansionPanelComponent } from './ExpansionPanel/expansion-panel.component';
import { GMExpansionPanelStackComponent } from './ExpansionPanelStack/expansion-panel-stack.component';
import { GMScrollableContentComponent } from './ScrollableContent/scrollable-content.component';
import { GMImageUploaderComponent } from './ImageUploader/image-uploader.component';
import { GMInputCollectionComponent } from './InputCollection/input-collection.component';

@NgModule({
    declarations: [
        GMButtonComponent,
        GMCheckboxComponent,
        GMTextFieldComponent,
        GMModalWindow,
        GMDropListComponent,
        GMDropListWindowComponent,
        GMDatePikerComponent,
        GMCalendarPopupComponent,
        GMCalendarDaysPartComponent,
        GMCalendarYearPartComponent,
        GMCalendarMonthPartComponent,
        GMDateCellComponent,
        GMCarouselComponent,
        GMCarouselItemComponent,
        GMHeaderComponent,
        GMLoaderComponent,
        GMExpansionPanelComponent,
        GMExpansionPanelStackComponent,
        GMScrollableContentComponent,
        GMImageUploaderComponent,
        GMInputCollectionComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
    ],
    entryComponents: [
        GMModalWindow,
        GMDropListWindowComponent,
        GMCalendarPopupComponent
    ],
    providers: [GMModelWindowService],
    exports: [
        GMButtonComponent,
        GMTextFieldComponent,
        GMDropListComponent,
        GMCheckboxComponent,
        GMDatePikerComponent,
        GMCarouselComponent,
        GMCarouselItemComponent,
        GMHeaderComponent,
        GMLoaderComponent,
        GMExpansionPanelComponent,
        GMExpansionPanelStackComponent,
        GMScrollableContentComponent,
        GMImageUploaderComponent,
        GMInputCollectionComponent
    ]
})
export class GodModeControlsModule { }
