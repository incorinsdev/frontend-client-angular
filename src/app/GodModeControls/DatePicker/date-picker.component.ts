import { Component, Input, HostListener, ElementRef, AfterViewInit, OnInit, ViewChild, Optional, Self } from '@angular/core';
import { ControlValueAccessor, NgControl, FormControl } from '@angular/forms';
import { ValueAccessorFunctions } from '../Shared/value-accessor-funcs.class';
import { GMModelWindowService } from '../ModalWindow/modal.service';
import { GMModalWindowPosition } from '../ModalWindow/modal-window-position.model';
import { GMMaskFormatter } from '../TextField/mask-formatter.class';
import { GMDateStringFormatter } from '../Shared/date-string-formatter.class';
import { KeyCodes } from '../Shared/keycodes.enum';
import { FieldError } from '../TextField/text-field.component';
import { GMDateFunctions } from '../Shared/date.functions';

const CALENDAR_POPUP_WIDTH = 295;
const CONTROL_HEIGHT = 65;

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gm-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['../Styles/gm-styles.scss'],
})
export class GMDatePikerComponent implements ControlValueAccessor, AfterViewInit, OnInit {

  @ViewChild('input') inputRef: ElementRef;

  @Input() tabInd = 1;
  @Input() placeholder = '';
  @Input() label = '';
  @Input() errors: Array<FieldError> = [];
  @Input() min: Date;
  @Input() max: Date;
  @Input() startDate = GMDateFunctions.today();
  @Input() mode = 'dateSelection';


  public hasError = false;
  public error = '';

  private _focused = false;
  get focused() { return this._focused; }
  set focused(val: boolean) {
    if (val !== this._focused) {
      this._focused = val;
      if (this.focused) {
        this.onTouchedCallback();
      } else { this.checkErrors(); }
    }
  }
  public buttonFocused = false;

  splash = '';

  private mask = '__.__.____';
  public maskFormatter: GMMaskFormatter = new GMMaskFormatter();

  public value: Date;
  public _disabled = false;

  private onChangeCallback: (_: any) => void = ValueAccessorFunctions.noop;
  private onTouchedCallback: () => void = ValueAccessorFunctions.noop;

  public stringValue = '';
  public remainigMask = this.mask;

  private _calendarWindowPosition: GMModalWindowPosition = new GMModalWindowPosition();


  @HostListener('window:resize', ['$event']) onWindowResizeHandler(event) {
    this.setWindowPosition();
  }
  @HostListener('window:scroll', ['$event']) onWindowScrollHandler(event) {
    this.setWindowPosition();
  }
  @HostListener('document:keydown', ['$event']) onKeyDownHandler(event: KeyboardEvent) {
    if ((event.keyCode === KeyCodes.Enter || event.keyCode === KeyCodes.Space) && this.buttonFocused) {
      event.preventDefault();
      this.openCalendar();
    }
  }

  setStringValue(date: string) {
    if (date !== this.maskFormatter.maskedValue) {
      if (date != null) {
        this.maskFormatter.maskedValue = date;
      } else {
        this.maskFormatter.maskedValue = '';
      }
      this.stringValue = this.maskFormatter.maskedValue;
      this.remainigMask = this.maskFormatter.remainingMask;
      this.writeValue(GMDateStringFormatter.convertStringToDate(this.stringValue));
    }
  }

  writeValue(obj: Date): void {
    if ((obj instanceof Date && obj !== this.value)) {
      this.value = obj;

      if (this.stringValue === '') {
        this.stringValue = GMDateStringFormatter.convertDateToString(this.value);
      }

      this.onChangeCallback(this.value);
      this.checkErrors();
    }
  }
  registerOnChange(fn: any): void { this.onChangeCallback = fn; }
  registerOnTouched(fn: any): void { this.onTouchedCallback = fn; }
  setDisabledState?(isDisabled: boolean): void { if (isDisabled !== this._disabled) { this._disabled = isDisabled; } }
  setWindowPosition() {
    this._calendarWindowPosition.x = this.elRef.nativeElement.getBoundingClientRect().right - CALENDAR_POPUP_WIDTH;
    this._calendarWindowPosition.ytop = this.elRef.nativeElement.getBoundingClientRect().bottom + 5;
    this._calendarWindowPosition.ybottom = this.elRef.nativeElement.getBoundingClientRect().top - 5;
  }

  ngAfterViewInit(): void { this.setWindowPosition(); }

  constructor(public elRef: ElementRef, public windowService: GMModelWindowService, @Optional() @Self() public controlDir: NgControl) {
    if (this.controlDir) { this.controlDir.valueAccessor = this; }
  }

  ngOnInit(): void {
    this.maskFormatter.input = this.inputRef;
    this.maskFormatter.mask = this.mask;
  }

  checkErrors() {
    this.hasError = false;
    if (this.controlDir) {
      if (+this.value < +this.min) {
        if (this.controlDir) {
          this.controlDir.control.setErrors({ 'min': true });
        }
      }
      if (+this.value > +this.max) {
        if (this.controlDir) {
          this.controlDir.control.setErrors({ 'max': true });
        }
      }
      this.hasError = !this.controlDir.valid;
      if (this.hasError) {
        this.errors.forEach(err => {
          if (this.controlDir.hasError(err.errorName)) {
            this.error = err.errorMessage;
          }
        });
      }
    } else {
      if (+this.value < +this.min) {
        this.hasError = true;
        this.error = 'Неверный формат даты';
      }
      if (+this.value > +this.max) {
        this.hasError = true;
        this.error = 'Неверный формат даты';
      }
    }
    if (this.value == null && this.stringValue) {
      this.hasError = true;
      this.error = 'Неверный формат даты';
    }
  }
  openCalendar() {
    this.setWindowPosition();
    this.windowService.openDatePickerWindow(
      this._calendarWindowPosition,
      this.value,
      this.startDate,
      this.max,
      this.min,
      this.mode
    ).window.ngOnClose.subscribe(x => {
      if (x) {
        this.maskFormatter.setNewCarretPosition(0);
        this.setStringValue(GMDateStringFormatter.convertDateToString(x));
      }
    });
  }

  pushToCheckErrors() {
    this.checkErrors();
    if (this.hasError && this.splash === '') {
      this.splash = 'splash';
      setTimeout(() => {
        this.splash = '';
      }, 800);
    }
  }
}
