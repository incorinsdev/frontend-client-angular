import { EventEmitter, Input, Output, Component, ViewChild, ElementRef } from '@angular/core';
import { YearCollection } from '../calendar.class';
import { GMDateFunctions } from '../../../Shared/date.functions';

const YEAR_COUNT = 35;

@Component({
  selector: 'gm-year-part',
  templateUrl: './year-part.component.html',
  styleUrls: ['../SharedCalendarStyles/shared-calendar.styles.scss'],
})
export class GMCalendarYearPartComponent {

  @Input() yearCollection: YearCollection = new YearCollection(2000, YEAR_COUNT);
  @Input() selectedDate: Date;
  @Input() selectedYear: number;
  @Input() minDate: Date;
  @Input() maxDate: Date;


  private _isFocused = false;

  @ViewChild('button') button: ElementRef;


  @Input() focusedDate: Date;
  @Input() get isFocused() { return this._isFocused; }
  set isFocused(val: boolean) {
    this._isFocused = val;
    if (!val && this.focusedDate.getFullYear() <= this.yearCollection.maxYear && this.focusedDate.getFullYear() >= this.yearCollection.minYear) {
      this.button.nativeElement.focus();
    }
  }

  @Output() onYearSelect: EventEmitter<number> = new EventEmitter<number>();
  @Output() onYearClick: EventEmitter<void> = new EventEmitter<void>();
  @Output() onFocusChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  selectYear(event: number) {
    this.selectedYear = event;
    this.onYearSelect.emit(this.selectedYear);
  }

  isThisYear(year: number) { return year != null ? year == GMDateFunctions.today().getFullYear() : false; }
  isYearSelected(year: number) {
    return (year != null && this.selectedDate != null) ? this.selectedDate.getFullYear() == year : false;
  }

  canFocusDate(year) { return this.isFocused && year == this.focusedDate.getFullYear(); }

  isYearDisabled(v: number): boolean {
    return v != null && ((this.minDate != null && v < this.minDate.getFullYear()) || (this.maxDate != null && v > this.maxDate.getFullYear()));
  }
  yearClick() { this.onYearClick.emit(); }

  clickOnCalendar() {
    this.isFocused = true;
    this.onFocusChange.emit(this.isFocused);
  }

}
