import { EventEmitter, Input, Output, Component, ViewChild, ElementRef } from '@angular/core';
import { Calendar, CalendarDay } from '../calendar.class';
import { GMDateFunctions } from '../../../Shared/date.functions';

@Component({
  selector: 'gm-days-part',
  templateUrl: './days-part.component.html',
  styleUrls: ['../SharedCalendarStyles/shared-calendar.styles.scss'],
})
export class GMCalendarDaysPartComponent {


  @Input() calendar: Calendar = new Calendar(1, 1, 2000);
  @Input() minDate: Date;
  @Input() maxDate: Date;
  @Input() selectedDate: Date;


  @ViewChild('button') button: ElementRef;

  private _isFocused = false;

  @Input() focusedDate: Date;
  @Input() get isFocused() { return this._isFocused; }
  set isFocused(val: boolean) {
    this._isFocused = val;
    if (!val && this.focusedDate.getMonth() === this.calendar.monthNumber) {
      this.button.nativeElement.focus();
    }
  }

  @Output() onDateSelect: EventEmitter<Date> = new EventEmitter<Date>();
  @Output() onMonthClick: EventEmitter<void> = new EventEmitter<void>();
  @Output() onFocusChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  selectDate(event: CalendarDay) {
    this.selectedDate = event.date;
    this.onDateSelect.emit(event.date);
  }

  canFocusDate(day) { return this.isFocused && day && GMDateFunctions.datesAreEqual(day.date, this.focusedDate); }
  compareSelected(day) { return day != null ? GMDateFunctions.datesAreEqual(this.selectedDate, day.date) : false }

  isToday(date: Date) { return GMDateFunctions.datesAreEqual(date, GMDateFunctions.today()); }

  isDateDisabled(v: CalendarDay): boolean {
    if (v != null && v.date != null) {
      if (this.minDate != null && +v.date < +this.minDate) {
        return true;
      }
      if (this.maxDate != null && +v.date > +this.maxDate) {
        return true;
      }
      return false;
    } else { return true; }
  }

  monthClick() { this.onMonthClick.emit(); }

  clickOnCalendar() {
    this.isFocused = true;
    this.onFocusChange.emit(this.isFocused);
  }

}
