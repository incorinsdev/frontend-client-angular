
const WEEK_DAYS_ARRAY = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
const MONTH_ARRAY = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
const START_YEAR_FOR_SELECTION = 2000;

export class CalendarDay {
  constructor(public date: Date, private firstDayOfWeekNumber: number) { }
  get weekDay(): string {
    return WEEK_DAYS_ARRAY[this.date.getDay()];
  }
  get monthDay(): number {
    return this.date.getDate();
  }
  get weekDayNumber(): number {
    const newDayNumber = this.date.getDay() - this.firstDayOfWeekNumber;
    if (newDayNumber >= 0) { return newDayNumber; } else { return 7 + newDayNumber; }
  }
  get month(): string {
    return MONTH_ARRAY[this.date.getMonth()];
  }
  get monthNumber(): number {
    return this.date.getMonth();
  }
  get isWeekend(): boolean {
    return this.date.getDay() === 0 || this.date.getDay() === 6;
  }
}

export class Calendar {
  constructor(public firstDayOfWeekNumber: number, public monthNumber: number, public yearNumber: number) {
    this.createCalendarWeeks();
    this.createWeekDayLabels();
  }
  days: Array<CalendarDay> = [];
  weekDayLabels: Array<string> = [];
  get monthName() {
    return MONTH_ARRAY[this.monthNumber];
  }
  get year() {
    return this.yearNumber;
  }

  createCalendarWeeks() {
    this.days = [];
    const date = new CalendarDay(new Date(this.yearNumber, this.monthNumber, 1, 0, 0, 0, 0), this.firstDayOfWeekNumber);
    const fd = date.weekDayNumber;
    for (let i = 0; i < fd; i++) {
      this.days.push(null);
    }
    while (date.monthNumber === this.monthNumber) {
      this.days.push(new CalendarDay(new Date(this.yearNumber, this.monthNumber, date.monthDay, 0, 0, 0, 0), this.firstDayOfWeekNumber));
      date.date.setDate(date.monthDay + 1);
    }
    while (this.days.length < 42) {
      this.days.push(null);
    }
  }
  createWeekDayLabels() {
    for (let i = this.firstDayOfWeekNumber; i < 7; i++) {
      this.weekDayLabels.push(WEEK_DAYS_ARRAY[i]);
    }
    for (let i = 0; i < this.firstDayOfWeekNumber; i++) {
      this.weekDayLabels.push(WEEK_DAYS_ARRAY[i]);
    }
  }
}
export class YearCollection {
  constructor(public currentYear: number, count: number) {
    const diff = Math.floor((currentYear - START_YEAR_FOR_SELECTION) / count);
    const startYear = START_YEAR_FOR_SELECTION + diff * count;
    this.years = [];
    for (let i = startYear; i < startYear + count; i++) {
      this.years.push(i);
    }
  }
  years: Array<number> = [];
  getYearLimits(): string {
    return this.years[0] + ' - ' + this.years[this.years.length - 1];
  }
  get maxYear() { return this.years[this.years.length - 1]; }
  get minYear() { return this.years[0]; }
}

export class Month {
  monthShortName: string;
  monthNumber: number;
  monthFullName: string;
  monthFullNameSCL: string;
}

export class MonthCollection {
  months: Array<Month> = [
    {
      monthNumber: 0,
      monthShortName: 'ЯНВ',
      monthFullName: 'Январь',
      monthFullNameSCL: 'Января'
    },
    {
      monthNumber: 1,
      monthShortName: 'ФЕВ',
      monthFullName: 'Февраль',
      monthFullNameSCL: 'Февраля'
    },
    {
      monthNumber: 2,
      monthShortName: 'МАР',
      monthFullName: 'Март',
      monthFullNameSCL: 'Марта'
    },
    {
      monthNumber: 3,
      monthShortName: 'АПР',
      monthFullName: 'Апрель',
      monthFullNameSCL: 'Апреля'
    },
    {
      monthNumber: 4,
      monthShortName: 'МАЙ',
      monthFullName: 'Май',
      monthFullNameSCL: 'Мая'
    },
    {
      monthNumber: 5,
      monthShortName: 'ИЮН',
      monthFullName: 'Июнь',
      monthFullNameSCL: 'Июня'
    },
    {
      monthNumber: 6,
      monthShortName: 'ИЮЛ',
      monthFullName: 'Июль',
      monthFullNameSCL: 'Июля'
    },
    {
      monthNumber: 7,
      monthShortName: 'АВГ',
      monthFullName: 'Август',
      monthFullNameSCL: 'Августа'
    },
    {
      monthNumber: 8,
      monthShortName: 'СЕН',
      monthFullName: 'Сентябрь',
      monthFullNameSCL: 'Сентября'
    },
    {
      monthNumber: 9,
      monthShortName: 'ОКТ',
      monthFullName: 'Октябрь',
      monthFullNameSCL: 'Октября'
    },
    {
      monthNumber: 10,
      monthShortName: 'НОЯ',
      monthFullName: 'Ноябрь',
      monthFullNameSCL: 'Ноября'
    },
    {
      monthNumber: 11,
      monthShortName: 'ДЕК',
      monthFullName: 'Декабрь',
      monthFullNameSCL: 'Декабря'
    }
  ];
}
