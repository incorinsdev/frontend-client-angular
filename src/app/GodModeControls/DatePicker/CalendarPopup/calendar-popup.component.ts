import {
  EventEmitter,
  Input,
  Output,
  Component,
  ViewChild,
  ElementRef,
  HostListener,
  AfterViewInit
} from '@angular/core';
import { Calendar, YearCollection, MonthCollection } from './calendar.class';
import { GMCalendarWindowAnimations } from './calendar.animations';
import { GMModalWindowContent } from '../../ModalWindow/modal-window-content.class';
import { GMCarouselComponent } from '../../Carousel/carousel.component';
import { KeyCodes } from '../../Shared/keycodes.enum';


const YEAR_COUNT = 35;

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gm-calendar-popup',
  templateUrl: './calendar-popup.component.html',
  styleUrls: ['./calendar-popup.component.scss'],
  animations: [GMCalendarWindowAnimations.slideCalendarAnimation]
})
export class GMCalendarPopupComponent extends GMModalWindowContent implements AfterViewInit {


  monthAnimationState = GMCalendarWindowAnimations.animationState;

  date: Date;
  calendars: Array<Calendar> = [];
  years: Array<YearCollection> = [];
  months: MonthCollection = new MonthCollection();
  textYears: Array<number> = [];
  curentPosition = 1;
  mode: 'dateSelection' | 'monthSelection' | 'yearSelection' = 'dateSelection';
  calendarFocused = true;

  private _startDate: Date;
  private selectedMonth: number;
  private _selectedYear: number;

  private _focusedDate: Date;
  get focusedDate() {
    return this._focusedDate
      ? this._focusedDate
      : (this.date ? this.date : this.startDate);
  }
  set focusedDate(val: Date) {
    if (val !== this._focusedDate) {
      this._focusedDate = val;
    }
  }

  @Input()
  get selectedYear() {
    return this._selectedYear;
  }
  set selectedYear(v: number) {
    if (v !== this._selectedYear && v != null) {
      this._selectedYear = v;
      this.years = [];
      for (let i = -1; i < 2; i++) {
        const newYearCollection = new YearCollection(this._selectedYear + YEAR_COUNT * i, YEAR_COUNT);
        this.years.push(newYearCollection);
      }
      this.textYears = [];
      this.textYears.push(this._selectedYear - 1);
      this.textYears.push(this._selectedYear);
      this.textYears.push(this._selectedYear + 1);
    }
  }


  @Input() firstDayOfWeek = 1;
  @Input() minDate: Date;
  @Input() maxDate: Date;

  @Output() ngOnSelect = new EventEmitter<Date>();

  @ViewChild('triangletop') triangletop: ElementRef;
  @ViewChild('trianglebottom') trianglebottom: ElementRef;
  @ViewChild(GMCarouselComponent) carousel: GMCarouselComponent;

  @Input()
  get startDate(): Date {
    return this._startDate;
  }
  set startDate(v: Date) {
    if (v !== this._startDate && v != null) {
      this._startDate = v;
      this.calendars = [];
      for (let i = -1; i < 2; i++) {
        const newDate = new Date(this._startDate);
        newDate.setMonth(this._startDate.getMonth() + i, 1);
        this.calendars.push(new Calendar(this.firstDayOfWeek, newDate.getMonth(), newDate.getFullYear()));
      }
      this.selectedYear = this._startDate.getFullYear();
      this.selectedMonth = this._startDate.getMonth();
    }
  }

  @HostListener('document:keydown', ['$event']) onKeyUpHandler(event: KeyboardEvent) {
    event.preventDefault();
    if (event.keyCode === KeyCodes.Escape) {
      this.closeWithoutData();
    } else {
      if (this.calendarFocused) {
        if (this.mode === 'dateSelection') {
          switch (event.keyCode) {
            case KeyCodes.Up:
              this.focusedDate.setDate(this.focusedDate.getDate() - 7);
              break;
            case KeyCodes.Down:
              this.focusedDate.setDate(this.focusedDate.getDate() + 7);
              break;
            case KeyCodes.Left:
              this.focusedDate.setDate(this.focusedDate.getDate() - 1);
              break;
            case KeyCodes.Right:
              this.focusedDate.setDate(this.focusedDate.getDate() + 1);
              break;
            case KeyCodes.Enter:
              this.selectDate(this.focusedDate);
              break;
            case KeyCodes.Tab:
              this.calendarFocused = false;
              break;
          }
        } else if (this.mode === 'yearSelection') {
          switch (event.keyCode) {
            case KeyCodes.Up:
              this.focusedDate.setFullYear(this.focusedDate.getFullYear() - 5);
              break;
            case KeyCodes.Down:
              this.focusedDate.setFullYear(this.focusedDate.getFullYear() + 5);
              break;
            case KeyCodes.Left:
              this.focusedDate.setFullYear(this.focusedDate.getFullYear() - 1);
              break;
            case KeyCodes.Right:
              this.focusedDate.setFullYear(this.focusedDate.getFullYear() + 1);
              break;
            case KeyCodes.Enter:
              this.selectYear(this.focusedDate.getFullYear());
              break;
            case KeyCodes.Tab:
              this.calendarFocused = false;
              break;
          }
        } else if (this.mode === 'monthSelection') {
          switch (event.keyCode) {
            case KeyCodes.Up:
              this.focusedDate.setMonth(this.focusedDate.getMonth() - 3);
              break;
            case KeyCodes.Down:
              this.focusedDate.setMonth(this.focusedDate.getMonth() + 5);
              break;
            case KeyCodes.Left:
              this.focusedDate.setMonth(this.focusedDate.getMonth() - 1);
              break;
            case KeyCodes.Right:
              this.focusedDate.setMonth(this.focusedDate.getMonth() + 1);
              break;
            case KeyCodes.Enter:
              this.selectMonth(this.focusedDate.getMonth());
              break;
            case KeyCodes.Tab:
              this.calendarFocused = false;
              break;
          }
        }
      } else {
        if (this.mode === 'dateSelection') {
          switch (event.keyCode) {
            case KeyCodes.Down:
              this.calendarFocused = true;
              break;
            case KeyCodes.Left:
              this.focusedDate.setMonth(this.focusedDate.getMonth() - 1);
              break;
            case KeyCodes.Right:
              this.focusedDate.setMonth(this.focusedDate.getMonth() + 1);
              break;
            case KeyCodes.Enter:
              this.mode = 'yearSelection';
              break;
            case KeyCodes.Tab:
              this.calendarFocused = true;
              break;
          }
        } else if (this.mode === 'yearSelection') {
          switch (event.keyCode) {
            case KeyCodes.Down:
              this.calendarFocused = true;
              break;
            case KeyCodes.Left:
              this.focusedDate.setFullYear(this.focusedDate.getFullYear() - YEAR_COUNT);
              break;
            case KeyCodes.Right:
              this.focusedDate.setFullYear(this.focusedDate.getFullYear() + YEAR_COUNT);
              break;
            case KeyCodes.Enter:
              this.mode = 'monthSelection';
              break;
            case KeyCodes.Tab:
              this.calendarFocused = true;
              break;
          }
        } else if (this.mode === 'monthSelection') {
          switch (event.keyCode) {
            case KeyCodes.Down:
              this.calendarFocused = true;
              break;
            case KeyCodes.Left:
              this.focusedDate.setMonth(this.focusedDate.getMonth() - 12);
              break;
            case KeyCodes.Right:
              this.focusedDate.setMonth(this.focusedDate.getMonth() + 12);
              break;
            case KeyCodes.Enter:
              this.mode = 'dateSelection';
              break;
            case KeyCodes.Tab:
              this.calendarFocused = true;
              break;
          }
        }
      }
      this.startDate = new Date(this.focusedDate);
    }
  }

  onCalendarAnimationDone(event) {
    if (this.mode === 'dateSelection') {
      if (event === 0) {
        const newDate = new Date(this._startDate);
        newDate.setMonth(this.startDate.getMonth() - 1, 1);
        this.startDate = newDate;
      } else if (event === 2) {
        const newDate = new Date(this._startDate);
        newDate.setMonth(this.startDate.getMonth() + 1, 1);
        this.startDate = newDate;
      }
    } else if (this.mode === 'yearSelection') {
      if (event === 0) {
        this.selectedYear = this.selectedYear - YEAR_COUNT;
      } else if (event === 2) {
        this.selectedYear = this.selectedYear + YEAR_COUNT;
      }
    } else if (this.mode === 'monthSelection') {
      if (event === 0) {
        this.selectedYear = this.selectedYear - 1;
      } else if (event === 2) {
        this.selectedYear = this.selectedYear + 1;
      }
    }
    this.focusedDate.setFullYear(this.startDate.getFullYear(), this.startDate.getMonth());
    this.carousel.setPosition(1);
    this.curentPosition = 1;
  }
  ngAfterViewInit(): void {
    this.carousel.setPosition(1);
    this.curentPosition = 1;
    this.focusedDate = this.startDate;
    if (this.window.verticalPosition === 'top') {
      this.renderer.setStyle(this.triangletop.nativeElement, 'opacity', '1');
      this.renderer.setStyle(this.trianglebottom.nativeElement, 'opacity', '0');
    } else {
      this.renderer.setStyle(this.triangletop.nativeElement, 'opacity', '0');
      this.renderer.setStyle(this.trianglebottom.nativeElement, 'opacity', '1');
    }
    this.window.ngPositionChange.subscribe(x => {
      if (x === 'top') {
        this.renderer.setStyle(this.triangletop.nativeElement, 'opacity', '1');
        this.renderer.setStyle(this.trianglebottom.nativeElement, 'opacity', '0');
      } else if (x === 'bottom') {
        this.renderer.setStyle(this.triangletop.nativeElement, 'opacity', '0');
        this.renderer.setStyle(this.trianglebottom.nativeElement, 'opacity', '1');
      }
    });
  }
  changeMode(mode) {
    this.mode = mode;
    if (mode === 'dateSelection') {
      this.selectedYear = this._startDate.getFullYear();
    }
  }
  selectDate(event) {
    this.date = new Date(event);
    this.selectedYear = this.date.getFullYear();
    this.selectedMonth = this.date.getMonth();
    this.closeWithData();
  }
  selectYear(v: number) {
    if (v != null) {
      this.selectedYear = v;
      this.mode = 'monthSelection';
    }
  }
  selectMonth(v: number) {
    if (v != null) {
      this.selectedMonth = v;
      const newDate = new Date(this._startDate);
      newDate.setMonth(this._startDate.getMonth(), 1);
      newDate.setFullYear(this.selectedYear, this.selectedMonth, 1);
      this.startDate = newDate;
      this.mode = 'dateSelection';
    }
  }
  closeWithoutData() {
    this.window.close();
  }
  closeWithData() {
    this.window.closeResult = this.date;
    this.window.close();
  }
}
