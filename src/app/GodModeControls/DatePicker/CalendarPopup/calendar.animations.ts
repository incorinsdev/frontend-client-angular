import { trigger, style, animate, state, transition } from '@angular/animations';

const ANIMATION_TIMINGS = '300ms cubic-bezier(0.25, 0.8, 0.25, 1)';

export class GMCalendarWindowAnimations {
    static animationState: 'void' | 'left' | 'right' = 'void';
    static slideCalendarAnimation = trigger('slideCalendar', [
        state('void', style({ transform: 'translate3d(-295px, 0, 0)' })),
        state('left', style({ transform: 'translate3d(-590px, 0, 0)' })),
        state('right', style({ transform: 'translate3d(0, 0, 0)' })),
        transition('* => left', animate(ANIMATION_TIMINGS)),
        transition('* => right', animate(ANIMATION_TIMINGS)),
    ]);
}
