import { EventEmitter, Input, Output, Component, ViewChild, ElementRef } from '@angular/core';
import { MonthCollection, Month } from '../calendar.class';
import { GMDateFunctions } from '../../../Shared/date.functions';

const YEAR_COUNT = 35;

@Component({
  selector: 'gm-month-part',
  templateUrl: './month-part.component.html',
  styleUrls: ['../SharedCalendarStyles/shared-calendar.styles.scss'],
})
export class GMCalendarMonthPartComponent {
  months: MonthCollection = new MonthCollection();

  @Input() month: Month;
  @Input() year: number;
  @Input() selectedDate: Date;
  @Input() selectedYear: number;

  @Input() minDate: Date;
  @Input() maxDate: Date;


  @ViewChild('button') button: ElementRef;

  private _isFocused = false;

  @Input() focusedDate: Date;
  @Input() get isFocused() { return this._isFocused; }
  set isFocused(val: boolean) {
    this._isFocused = val;
    if (!val && this.focusedDate.getFullYear() == this.year) {
      this.button.nativeElement.focus();
    }
  }

  @Output() onMonthSelect: EventEmitter<Month> = new EventEmitter<Month>();
  @Output() onYearClick: EventEmitter<void> = new EventEmitter<void>();
  @Output() onFocusChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  selectMonth(event: Month) {
    this.month = event;
    this.onMonthSelect.emit(this.month);
  }

  isThisMonth(m: Month) { return m != null ? (m.monthNumber == GMDateFunctions.today().getMonth() && this.year == GMDateFunctions.today().getFullYear()) : false; }
  isMonthSelected(m: Month) {
    return (m != null && this.selectedDate != null) ? this.selectedDate.getMonth() == m.monthNumber && this.year == this.selectedDate.getFullYear() : false;
  }
  canFocusMonth(month) { return this.isFocused && month && month.monthNumber == this.focusedDate.getMonth(); }

  isMonthDisabled(m: Month): boolean {
    return m != null && ((this.minDate != null && m.monthNumber < this.minDate.getMonth() && this.selectedYear <= this.minDate.getFullYear()) ||
      (this.maxDate != null && m.monthNumber > this.maxDate.getMonth() && this.selectedYear >= this.maxDate.getFullYear()));
  }
  yearClick() { this.onYearClick.emit(); }

  clickOnCalendar() {
    this.isFocused = true;
    this.onFocusChange.emit(this.isFocused);
  }

}
