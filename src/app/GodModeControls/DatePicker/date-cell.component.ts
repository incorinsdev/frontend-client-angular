import { EventEmitter, Input, Output, Component, ElementRef, ViewChild } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gm-date-cell',
  templateUrl: './date-cell.component.html',
  styleUrls: ['./date-cell.component.scss'],
})
export class GMDateCellComponent {
  @Input() value: any;
  @Input() type: 'number' | 'date' | 'month' = 'date';
  @Input() isSelected = false;
  @Input() disabled = false;
  @Input() isCurrent = false;

  @ViewChild('cell') cellRef: ElementRef;


  private _isFocused = false;
  @Input() get isFocused() { return this._isFocused; }
  set isFocused(val: boolean) {
    if (!this.disabled) {
      this._isFocused = val;
      if (val && this.cellRef) {
        this.cellRef.nativeElement.setAttribute('tabIndex', 1);
      }
    }
  }

  @Output() ngOnSelect = new EventEmitter<any>();

  get cellText() {
    if (this.value != null) {
      if (this.type === 'date') {
        return this.value.monthDay;
      } else if (this.type === 'month') {
        return this.value.monthFullName;
      } else {
        return this.value;
      }
    }
    return '';
  }

  select() {
    if (!this.disabled && this.value != null) {
      this.ngOnSelect.emit(this.value);
    }
  }

  get class(): string {
    let result = '';
    if (this.value == null) { result = result + ' empty'; }
    if (this.isSelected) { result = result + ' selected'; }
    if (this.disabled) { result = result + ' disabled'; }
    if (this.isCurrent) { result = result + ' current'; }
    if (this.isFocused) { result = result + ' focused'; }
    if (this.type != null) { result = result + ' ' + this.type; }
    return result;
  }
}
