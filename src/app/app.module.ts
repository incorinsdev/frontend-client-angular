import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';

import { GodModeControlsModule } from './GodModeControls/god-mode-controls.module';
import { SerialNumberTextfieldComponent } from './appControls/serial-number-textfield/serial-number-textfield.component';

import { APIClient } from './services/API/api.service';
import { CrossPageDataService } from './services/cross-page-data.service';

import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { DotsComponent } from './shared/dots/dots.component';
import { CoptersComponent } from './shared/copters/copters.component';
import { LimitBlockComponent } from './shared/limitBlock/limit-block/limit-block.component';

import { MainPageComponent } from './pages/main-page/main-page.component';
import { CalcPageComponent } from './pages/calc-page/calc-page.component';
import { OrderPageComponent } from './pages/order-page/order-page.component';
import { AboutPageComponent } from './pages/about-page/about-page.component';
import { ContactsPageComponent } from './pages/contacts-page/contacts-page.component';
import { InsCopterPageComponent } from './pages/ins-copter-page/ins-copter-page.component';
import { InsCopterComponent } from './shared/insCopter/insCopter.component';
import { PointItemComponent } from './shared/point-item/point-item.component';
import { PaymentPageComponent } from './pages/payment-page/payment-page.component';
import { InsuredEventPageComponent } from './pages/insured-event-page/insured-event-page.component';
import { DocumentPageComponent } from './pages/document-page/document-page.component';
import { QuestionPageComponent } from './pages/question-page/question-page.component';
import { RepairCityPageComponent } from './pages/repair-city-page/repair-city-page.component';
import { CitiesComponent } from './shared/cities/cities.component';
import { ShopComponent } from './shared/shop/shop.component';
import { SellerCityPageComponent } from './pages/seller-city-page/seller-city-page.component';
import { PayerComponent } from './shared/payer/payer.component';
import { ModelBlockComponent } from './shared/modelBlock/modelBlock.component';
import { PolicyCheckboxComponent } from './appControls/policy-checkbox/policy-checkbox.component';
import { InsurenceRequestWindowComponent } from './windows/insurence-request-window/insurence-request-window.component';
import { PaymentFailPageComponent } from './pages/payment-fail-page/payment-fail-page.component';
import { PaymentSuccessPageComponent } from './pages/payment-success-page/payment-success-page.component';
import { PaymentStatusComponent } from './shared/payment-status/payment-status.component';
import { SideBarComponent } from './shared/sidebar/side-bar.component';

const appRoutes: Routes = [
  { path: '', component: MainPageComponent },
  { path: 'repair', component: RepairCityPageComponent },
  { path: 'shop', component: SellerCityPageComponent },
  { path: 'about', component: AboutPageComponent },
  { path: 'docs', component: DocumentPageComponent },
  { path: 'payment', component: PaymentPageComponent },
  { path: 'faq', component: QuestionPageComponent },
  { path: 'contacts', component: ContactsPageComponent },
  { path: 'insured-event', component: InsuredEventPageComponent },
  { path: 'liability-insurance', component: CalcPageComponent },
  { path: 'property-insurance', component: InsCopterPageComponent },
  { path: 'order', component: OrderPageComponent },
  { path: 'success', component: PaymentSuccessPageComponent },
  { path: 'fail', component: PaymentFailPageComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MainPageComponent,
    DotsComponent,
    CoptersComponent,
    AboutPageComponent,
    ContactsPageComponent,
    PaymentPageComponent,
    DocumentPageComponent,
    InsuredEventPageComponent,
    PaymentFailPageComponent,
    PaymentSuccessPageComponent,
    QuestionPageComponent,
    CalcPageComponent,
    LimitBlockComponent,
    OrderPageComponent,
    SerialNumberTextfieldComponent,
    InsCopterPageComponent,
    InsCopterComponent,
    PointItemComponent,
    RepairCityPageComponent,
    CitiesComponent,
    ShopComponent,
    SellerCityPageComponent,
    PayerComponent,
    ModelBlockComponent,
    PolicyCheckboxComponent,
    InsurenceRequestWindowComponent,
    PaymentStatusComponent,
    SideBarComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    GodModeControlsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes, { useHash: false }),
  ],
  entryComponents: [InsurenceRequestWindowComponent],
  providers: [CrossPageDataService, APIClient],
  bootstrap: [AppComponent]
})
export class AppModule { }
