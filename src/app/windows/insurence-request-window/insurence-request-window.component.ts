import { Component, ViewChild } from '@angular/core';
import { GMModalWindowContent } from '../../GodModeControls/ModalWindow/modal-window-content.class';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { GMRegularExpressions } from '../../GodModeControls/Shared/regexps.class';
import { FieldError, GMTextFieldComponent } from '../../GodModeControls/TextField/text-field.component';
import { PolicyCheckboxComponent } from '../../appControls/policy-checkbox/policy-checkbox.component';
import { GMButtonComponent } from '../../GodModeControls/Button/button.component';
import { InsRequest } from '../../services/API/models/ins-request.model';
import { CrossPageDataService } from '../../services/cross-page-data.service';

@Component({
  selector: 'app-insurence-request-window',
  templateUrl: './insurence-request-window.component.html',
  styleUrls: ['./insurence-request-window.component.scss']
})
export class InsurenceRequestWindowComponent extends GMModalWindowContent {

  informed = false;
  modelId: number;
  smService: CrossPageDataService;
  reqDone = false;
  reqSent = false;
  timer = 3;

  textFieldNameErrors: Array<FieldError> = [
    { errorName: 'required', errorMessage: 'Заполните поле' },
    { errorName: 'pattern', errorMessage: 'Только русские буквы' },
  ];
  emailFieldNameErrors: Array<FieldError> = [
    { errorName: 'required', errorMessage: 'Заполните поле' },
    { errorName: 'pattern', errorMessage: 'Неверный формат E-mail' },
  ];

  requestFormGroup = new FormGroup({
    firstNameControl: new FormControl('', [Validators.required, Validators.pattern(GMRegularExpressions.ONLY_RUSSIAN_LETTERS)]),
    emailControl: new FormControl('', [Validators.required, Validators.pattern(GMRegularExpressions.EMAIL_VALIDATION)]),
    cityControl: new FormControl('', [Validators.required, Validators.pattern(GMRegularExpressions.ONLY_RUSSIAN_LETTERS)]),
  });

  @ViewChild('firstNameControl') firstNameControl: GMTextFieldComponent;
  @ViewChild('cityControl') cityControl: GMTextFieldComponent;
  @ViewChild('emailPersControl') emailPersControl: GMTextFieldComponent;
  @ViewChild('policyControl') policyControl: PolicyCheckboxComponent;
  @ViewChild('sendButton') sendButton: GMButtonComponent;

  sendRequest() {
    if (this.requestFormGroup.valid && this.informed) {
      const req = new InsRequest();
      req.insModelId = this.modelId;
      req.name = this.firstNameControl.value;
      req.city = this.cityControl.value;
      req.email = this.emailPersControl.value;
      req.trick();
      this.reqSent = true;
      this.smService.sendInsRequest(req).subscribe(ok => { if (ok) { this.reqDone = true; this.setTimer(); } });
    } else {
      this.firstNameControl.pushToCheckErrors();
      this.emailPersControl.pushToCheckErrors();
      this.cityControl.pushToCheckErrors();
      if (!this.informed) {
        this.policyControl.pushToClick();
      }
      this.sendButton.shake();
    }
  }
  close() { this.window.close(); }

  setTimer() {
    if (this.timer > 0) {
      setTimeout(() => { this.timer--; this.setTimer(); }, 1000);
    } else {
      this.close();
    }
  }
}
