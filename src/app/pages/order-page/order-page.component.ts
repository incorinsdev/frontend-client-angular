import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CrossPageDataService } from '../../services/cross-page-data.service';
import { Limit } from '../../services/API/models/limit.model';
import { GMDateStringFormatter } from '../../GodModeControls/Shared/date-string-formatter.class';
import { PayerType } from '../../services/API/models/payerType.model';
import { Payer } from '../../shared/payer/payer.model';
import { Copter } from '../../services/API/models/copter.model';
import { GMStringFormatter } from '../../GodModeControls/Shared/string-formatter.class';
import { Order } from '../../services/API/models/order.model';
import { Policy } from '../../services/API/models/policy.model';
import { PayerComponent } from '../../shared/payer/payer.component';
import { PolicyCheckboxComponent } from '../../appControls/policy-checkbox/policy-checkbox.component';

@Component({
  selector: 'app-order-page',
  templateUrl: './order-page.component.html',
  styleUrls: ['./order-page.component.scss']
})
export class OrderPageComponent implements OnInit {


  limit: Limit;
  startDate: Date;
  payerType: PayerType;
  promoCode: string;

  get limitId() { return this.limit ? this.limit.id : null; }
  get stringStartDate() { return this.startDate ? GMDateStringFormatter.convertDateToString(this.startDate) : null; }

  payerValid = false;
  copterValid = false;
  informed = false;

  payer: Payer = new Payer();
  copter = new Copter();

  @ViewChild('payerControl') payerControl: PayerComponent;
  @ViewChild('copterControl') copterControl: PayerComponent;
  @ViewChild('policyControl') policyControl: PolicyCheckboxComponent;

  constructor(private route: ActivatedRoute, public cpdService: CrossPageDataService) {
    this.route.queryParams.subscribe(params => {
      if (params['startdate'] && params['id'] && params['payerType']) {
        this.startDate = GMDateStringFormatter.convertStringToDate(params['startdate']);
        this.payerType = Number(params['payerType']);
        this.promoCode = params['promoCode'];
        if (!this.startDate || !(this.payerType === 1 || this.payerType === 0)) {
          this.cpdService.redirectToNotFoundPage();
        } else {
          this.takeCareOfLimits(Number(params['id']));
        }

        if (this.promoCode && this.promoCode.length > 0) {
          this.cpdService.checkPromoCode(this.promoCode).subscribe();
        }
      } else {
        this.cpdService.redirectToNotFoundPage();
      }
    });
  }

  get stringPrice() {
    let price = this.limit ? this.cpdService.limits.getPrice(this.limit) : 0;
    const discount = price * this.cpdService.promoCode.ratio;
    price -= discount;
    return GMStringFormatter.addSpacesBetweenRanks(price, 0);
  }

  private takeCareOfLimits(id: number) {
    if (this.cpdService.limits.isLoaded) {
      this.limit = this.cpdService.limits.getLimitById(id);
    } else {
      this.cpdService.limits.loadComplete.subscribe(success => {
        this.limit = this.cpdService.limits.getLimitById(id);
      });
    }
  }

  get isAllOk() { return this.payerValid && this.copterValid && this.informed; }

  ngOnInit(): void { document.querySelector('html').scrollTop = 0; }

  sendPolicy() {
    if (this.isAllOk) {
      const order = new Order();
      if (this.payerType === PayerType.Entity) {
        order.companyPayer = this.payer.toCompany();
      } else {
        order.personPayer = this.payer.toPerson();
      }
      order.policy = new Policy();
      order.promoCode = this.promoCode;
      order.policy.copter = this.copter;
      order.policy.insuranceStartDate = GMDateStringFormatter.convertDateToFormattedString(this.startDate);
      order.policy.limitId = this.limitId;
      this.cpdService.sendPayRequest(order);
    } else {
      if (!this.payerValid) {
        this.payerControl.pushToCheckErrors();
      }
      if (!this.copterValid) {
        this.copterControl.pushToCheckErrors();
      }
      if (!this.informed) {
        this.policyControl.pushToClick();
      }
    }
  }
}
