import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-insured-event-page',
  templateUrl: './insured-event-page.component.html',
  styleUrls: ['./insured-event-page.component.scss']
})
export class InsuredEventPageComponent implements OnInit {
  constructor() { }
  ngOnInit(): void {
    document.querySelector('html').scrollTop = 0;
  }
}
