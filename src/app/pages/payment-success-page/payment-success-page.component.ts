import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-payment-success-page',
  templateUrl: './payment-success-page.component.html',
  styleUrls: ['./payment-success-page.component.scss']
})
export class PaymentSuccessPageComponent implements OnInit {

  email = '';

  constructor(private _activatedRoute: ActivatedRoute, private _router: Router) {
    _activatedRoute.queryParams.subscribe(
      params => this.email = params['email']
    );
  }

  ngOnInit(): void { document.querySelector('html').scrollTop = 0; }
}
