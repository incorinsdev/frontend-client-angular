import { Component, OnInit } from '@angular/core';
import { CrossPageDataService } from '../../services/cross-page-data.service';

@Component({
  selector: 'app-ins-copter-page',
  templateUrl: './ins-copter-page.component.html',
  styleUrls: ['./ins-copter-page.component.scss']
})
export class InsCopterPageComponent implements OnInit {
  get models() { return this.cpdService.insBrands.models; }
  get addedCrosses() {
    const a = this.cpdService.insBrands.models.length % 3;
    switch (a) {
      case 0: return [{}, {}, {}];
      case 1: return [{}];
      case 2: return [{}, {}];
    }
  }
  constructor(public cpdService: CrossPageDataService) { }
  ngOnInit(): void { document.querySelector('html').scrollTop = 0; }
}
