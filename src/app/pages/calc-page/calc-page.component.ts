import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';

import { DropListValue } from '../../GodModeControls/DropList/drop-list-value.model';
import { CurrencyType } from '../../services/API/models/currencyType.model';
import { GMDateFunctions } from '../../GodModeControls/Shared/date.functions';
import { FieldError, GMTextFieldComponent } from '../../GodModeControls/TextField/text-field.component';
import { GMDateStringFormatter } from '../../GodModeControls/Shared/date-string-formatter.class';
import { GMStringFormatter } from '../../GodModeControls/Shared/string-formatter.class';
import { Limit } from '../../services/API/models/limit.model';
import { CrossPageDataService } from '../../services/cross-page-data.service';
import { PayerType } from '../../services/API/models/payerType.model';
import { GMDatePikerComponent } from '../../GodModeControls/DatePicker/date-picker.component';
import { GMButtonComponent } from '../../GodModeControls/Button/button.component';

@Component({
  selector: 'app-calc-page',
  templateUrl: './calc-page.component.html',
  styleUrls: ['./calc-page.component.scss']
})
export class CalcPageComponent implements OnInit {

  currencyValues: Array<DropListValue>;
  currentCurrencyValue: DropListValue;

  payerTypeValues: Array<DropListValue>;
  currentPayerType: DropListValue;

  currentLimit: Limit;

  tomorrow = GMDateFunctions.tomorrow();
  tomorrowPlusMonth = GMDateFunctions.tomorrowInMonthPlus(1);
  startDateControl = new FormControl(GMDateFunctions.tomorrow(), [Validators.required]);
  endDateControl = new FormControl({
    value: GMDateStringFormatter.convertDateToString(GMDateFunctions.tomorrowInYearPlus(1)),
    disabled: true
  });
  promoCodeControl = new FormControl('', {updateOn: 'change'});
  dateFieldNameErrors: Array<FieldError> = [];

  private startDate: Date = this.tomorrow;
  private endDate: Date;

  @ViewChild('startDatePicker') startDatePicker: GMDatePikerComponent;
  @ViewChild('shakeBtn') shakeBtn: GMButtonComponent;
  @ViewChild('promoCode') promoCode: GMTextFieldComponent;

  get stringPrice() {
    let price = this.currentLimit ? this.cpdService.limits.getPrice(this.currentLimit) : 0;
    const discount = price * this.cpdService.promoCode.ratio;
    price -= discount;
    return GMStringFormatter.addSpacesBetweenRanks(price, 0);
  }

  constructor(private router: Router, public cpdService: CrossPageDataService) {
    this.createCurrencies();
    this.createPayerTypes();
    this.dateFieldNameErrors.push(new FieldError('required', 'Заполните поле'));
    this.dateFieldNameErrors.push(new FieldError('min', 'Не ранее завтрашнего дня'));
    this.dateFieldNameErrors.push(new FieldError('promoCodeValid', 'Промокод недействителен'));
    this.cpdService.promoCode.clear();
  }
  createCurrencies() {
    this.currencyValues = [];
    this.currencyValues.push(new DropListValue(CurrencyType.Rub, 'Рубли'));
    this.currencyValues.push(new DropListValue(CurrencyType.Euro, 'Евро'));
    this.currencyValues.push(new DropListValue(CurrencyType.Dollar, 'Доллары США'));
    this.currentCurrencyValue = this.currencyValues[0];
  }
  createPayerTypes() {
    this.payerTypeValues = [];
    this.payerTypeValues.push(new DropListValue(PayerType.Individual, 'Физ. Лицом'));
    this.payerTypeValues.push(new DropListValue(PayerType.Entity, 'Юр. Лицом'));
    this.currentPayerType = this.payerTypeValues[0];
  }
  changeDate(event) {
    if (event != null) {
      this.startDate = event;
      this.endDate = new Date(event);
      this.endDate.setFullYear(this.startDate.getFullYear() + 1);
      this.endDate.setDate(this.startDate.getDate() - 1);
      this.endDateControl.setValue(GMDateStringFormatter.convertDateToString(this.endDate));
    }
  }

  redirectToOrder() {
    if (this.startDateControl.invalid) {
      this.startDatePicker.pushToCheckErrors();
      this.shakeBtn.shake();
    } else if (this.promoCodeControl.invalid) {
    } else {
      this.router.navigate(['/order'], {
        queryParams: {
          id: this.currentLimit.id,
          startdate: GMDateStringFormatter.convertDateToString(this.startDate),
          payerType: this.currentPayerType.value,
          promoCode: this.promoCodeControl.value ? this.promoCodeControl.value : undefined
        }
      });
    }
  }

  changePromoCode(event: string) {
    if (event.length === 0) {
      this.cpdService.promoCode.clear();
    }
    this.cpdService.checkPromoCode(event);
  }

  ngOnInit(): void {
    document.querySelector('html').scrollTop = 0;
    this.promoCodeControl.valueChanges
      .pipe(debounceTime(1000))
      .subscribe(promoCode => {
        if (promoCode.length >= 2) {
          this.cpdService.checkPromoCode(promoCode).subscribe(value => {
            if (!value.isValid) {
              this.promoCodeControl.setErrors({ promoCodeValid: true });
            } else {
              this.promoCodeControl.setErrors(null);
            }
            this.promoCode.pushToCheckErrors();
          });
        }
      });
  }
}
