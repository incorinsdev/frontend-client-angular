import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-payment-fail-page',
  templateUrl: './payment-fail-page.component.html',
  styleUrls: ['./payment-fail-page.component.scss']
})
export class PaymentFailPageComponent implements OnInit {
  ngOnInit(): void { document.querySelector('html').scrollTop = 0; }
}
