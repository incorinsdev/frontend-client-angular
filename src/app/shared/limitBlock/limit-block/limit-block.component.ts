import { Component, Input, Output, EventEmitter, ViewChildren, QueryList, OnInit } from '@angular/core';
import { CrossPageDataService } from '../../../services/cross-page-data.service';
import { GMCheckboxComponent } from '../../../GodModeControls/Checkbox/checkbox.component';
import { GMStringFormatter } from '../../../GodModeControls/Shared/string-formatter.class';
import { CurrencyType } from '../../../services/API/models/currencyType.model';
import { Limit } from '../../../services/API/models/limit.model';

class LimitBoolean {
  limit: Limit;
  isSelected = false;
  constructor(limit?: Limit, isSelected?: boolean) {
    this.limit = limit;
    this.isSelected = isSelected;
  }
}

@Component({
  selector: 'app-limit-block',
  templateUrl: './limit-block.component.html',
  styleUrls: ['./limit-block.component.scss']
})
export class LimitBlockComponent implements OnInit {

  private _currency: CurrencyType;
  booleanValues: Array<LimitBoolean> = [];

  @ViewChildren('radiobox') radioboxes: QueryList<GMCheckboxComponent>;

  @Input() get currency() { return this._currency; }
  set currency(val: CurrencyType) {
    if (this._currency !== val) {
      this._currency = val;
      if (!this.cpdService.limits.isLoaded) {
        this.cpdService.limits.loadComplete.subscribe(x => { this.setArray(); });
      } else { this.setArray(); }
    }
  }

  @Output() ngOnValueChange: EventEmitter<Limit> = new EventEmitter<Limit>();
  constructor(public cpdService: CrossPageDataService) { }

  private fillbooleanArray(data: Array<Limit>) {
    this.booleanValues = [];
    data.forEach(element => { this.booleanValues.push(new LimitBoolean(element, false)); });
    if (this.booleanValues.length > 0) {
      this.booleanValues[0].isSelected = true;
      this.ngOnValueChange.emit(this.booleanValues[0].limit);
    } else { this.ngOnValueChange.emit(null); }
  }

  ngOnInit(): void {
    this.currency = CurrencyType.Rub;
    this.setArray();
  }


  private setArray() {
    if (this._currency === CurrencyType.Rub) {
      this.fillbooleanArray(this.cpdService.limits.limitsRUB);
    } else if (this._currency === CurrencyType.Euro) {
      this.fillbooleanArray(this.cpdService.limits.limitsEUR);
    } else if (this._currency === CurrencyType.Dollar) {
      this.fillbooleanArray(this.cpdService.limits.limitsUSD);
    }
  }

  setNewSelectedLimit(value: boolean, object: LimitBoolean) {
    if (!object.isSelected && value) {
      object.isSelected = value;
      this.booleanValues.filter(x => x !== object).forEach(x => { x.isSelected = false; });
      this.ngOnValueChange.emit(object.limit);
    } else if (object.isSelected && !value) {
      object.isSelected = true;
      value = true;
    }
  }

  getFocusClass(index: number) {
    let counter = -1;
    let result = '';
    if (this.radioboxes) {
      this.radioboxes.forEach(element => {
        counter++;
        if (element.focused && counter === index) { result = 'focused'; }
      });
    }
    return result;
  }

  formatNumber(value) {
    return GMStringFormatter.addSpacesBetweenRanks(value, 0);
  }

}
