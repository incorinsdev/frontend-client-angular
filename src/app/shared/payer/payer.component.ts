import { Component, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { FieldError, GMTextFieldComponent } from '../../GodModeControls/TextField/text-field.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { GMRegularExpressions } from '../../GodModeControls/Shared/regexps.class';
import { GMDateFunctions } from '../../GodModeControls/Shared/date.functions';
import { PayerType } from '../../services/API/models/payerType.model';
import { Payer } from './payer.model';
import { GMDatePikerComponent } from '../../GodModeControls/DatePicker/date-picker.component';
import { GMDateStringFormatter } from '../../GodModeControls/Shared/date-string-formatter.class';

@Component({
  selector: 'app-payer',
  templateUrl: './payer.component.html',
  styleUrls: ['./payer.component.scss']
})
export class PayerComponent {

  @Input() type: PayerType = PayerType.Entity;

  textFieldNameErrors: Array<FieldError> = [];
  emailFieldNameErrors: Array<FieldError> = [];
  dateFieldNameErrors: Array<FieldError> = [];
  innFieldNameErrors: Array<FieldError> = [];
  kppFieldNameErrors: Array<FieldError> = [];
  minBirthDate = GMDateFunctions.adultBirthDate();

  @Output() validSatatusChanged: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() payerModelChanges: EventEmitter<Payer> = new EventEmitter<Payer>();

  personFormGroup = new FormGroup({
    firstNameControl: new FormControl('', [Validators.required, Validators.pattern(GMRegularExpressions.ONLY_RUSSIAN_LETTERS)]),
    secondNameControl: new FormControl('', [Validators.required, Validators.pattern(GMRegularExpressions.ONLY_RUSSIAN_LETTERS)]),
    middleNameControl: new FormControl('', [Validators.pattern(GMRegularExpressions.ONLY_RUSSIAN_LETTERS)]),
    birthDateControl: new FormControl('', [Validators.required]),
    emailControl: new FormControl('', [Validators.required, Validators.pattern(GMRegularExpressions.EMAIL_VALIDATION)]),
    phoneControl: new FormControl(''),
  });

  companyFormGroup = new FormGroup({
    companyNameControl: new FormControl('', [Validators.required, Validators.pattern(GMRegularExpressions.ONLY_RUSSIAN_LETTERS)]),
    INNControl: new FormControl('', [Validators.required, Validators.maxLength(10), Validators.minLength(10)]),
    KPPControl: new FormControl('', [Validators.maxLength(9), Validators.minLength(9)]),
    emailControl: new FormControl('', [Validators.required, Validators.pattern(GMRegularExpressions.EMAIL_VALIDATION)]),
    phoneControl: new FormControl(''),
  });

  @ViewChild('firstNameControl') firstNameControl: GMTextFieldComponent;
  @ViewChild('secondNameControl') secondNameControl: GMTextFieldComponent;
  @ViewChild('birthDateControl') birthDateControl: GMDatePikerComponent;
  @ViewChild('emailPersControl') emailPersControl: GMTextFieldComponent;

  @ViewChild('companyNameControl') companyNameControl: GMTextFieldComponent;
  @ViewChild('innControl') innControl: GMTextFieldComponent;
  @ViewChild('kppControl') kppControl: GMTextFieldComponent;
  @ViewChild('emailCompControl') emailCompControl: GMTextFieldComponent;

  constructor() {
    this.setErrors();
    this.companyFormGroup.statusChanges.subscribe(x => { this.validSatatusChanged.emit(this.companyFormGroup.valid); });
    this.personFormGroup.statusChanges.subscribe(x => { this.validSatatusChanged.emit(this.personFormGroup.valid); });

    this.companyFormGroup.valueChanges.subscribe(x => {
      const res = new Payer();
      res.title = this.companyFormGroup.controls.companyNameControl.value;
      res.inn = this.companyFormGroup.controls.INNControl.value;
      res.kpp = this.companyFormGroup.controls.KPPControl.value;
      res.phone = this.companyFormGroup.controls.phoneControl.value;
      res.email = this.companyFormGroup.controls.emailControl.value;
      this.payerModelChanges.emit(res);
    });
    this.personFormGroup.valueChanges.subscribe(x => {
      const res = new Payer();
      res.firstName = this.personFormGroup.controls.firstNameControl.value;
      res.surname = this.personFormGroup.controls.secondNameControl.value;
      res.middleName = this.personFormGroup.controls.middleNameControl.value;
      res.birthDate = this.personFormGroup.controls.birthDateControl.value === ''
        ? this.personFormGroup.controls.birthDateControl.value
        : GMDateStringFormatter.convertDateToFormattedString(this.personFormGroup.controls.birthDateControl.value);
      res.phone = this.personFormGroup.controls.phoneControl.value;
      res.email = this.personFormGroup.controls.emailControl.value;
      this.payerModelChanges.emit(res);
    });
  }
  setErrors() {
    this.textFieldNameErrors.push(new FieldError('required', 'Заполните поле'));
    this.textFieldNameErrors.push(new FieldError('pattern', 'Только русские буквы'));
    this.dateFieldNameErrors.push(new FieldError('required', 'Заполните поле'));
    this.dateFieldNameErrors.push(new FieldError('max', 'Вам нет 18 лет'));

    this.innFieldNameErrors.push(new FieldError('required', 'Заполните поле'));
    this.innFieldNameErrors.push(new FieldError('pattern', 'Только цифры'));
    this.innFieldNameErrors.push(new FieldError('maxlength', 'ИНН содержит 10 цифр'));
    this.innFieldNameErrors.push(new FieldError('minlength', 'ИНН содержит 10 цифр'));
    this.kppFieldNameErrors.push(new FieldError('pattern', 'Только цифры'));
    this.kppFieldNameErrors.push(new FieldError('maxlength', 'КПП содержит 9 цифр'));
    this.kppFieldNameErrors.push(new FieldError('minlength', 'КПП содержит 9 цифр'));

    this.emailFieldNameErrors.push(new FieldError('required', 'Заполните поле'));
    this.emailFieldNameErrors.push(new FieldError('pattern', 'Неверный формат E-mail'));
  }
  pushToCheckErrors() {
    if (this.type === PayerType.Entity && this.companyFormGroup.invalid) {
      this.companyNameControl.pushToCheckErrors();
      this.innControl.pushToCheckErrors();
      this.kppControl.pushToCheckErrors();
      this.emailCompControl.pushToCheckErrors();
    } else if (this.type === PayerType.Individual && this.personFormGroup.invalid) {
      this.firstNameControl.pushToCheckErrors();
      this.secondNameControl.pushToCheckErrors();
      this.emailPersControl.pushToCheckErrors();
      this.birthDateControl.pushToCheckErrors();
    }
  }
}
