import { Company } from '../../services/API/models/company.model';
import { Person } from '../../services/API/models/person.model';

export class Payer {
    firstName!: string;
    middleName?: string | null;
    surname!: string;
    birthDate!: Date | String;

    title!: string;
    inn!: string;
    kpp!: string;

    email!: string;
    phone!: string;

    init(data?: any) {
        if (data) {
            if (data['firstName']) {
                this.firstName = data['firstName'] !== undefined ? data['firstName'] : <any>null;
                this.middleName = data['middleName'] !== undefined ? data['middleName'] : <any>null;
                this.surname = data['surname'] !== undefined ? data['surname'] : <any>null;
                this.birthDate = data['birthDate'] ? new Date(data['birthDate'].toString()) : <any>null;
            } else {
                this.title = data['title'] !== undefined ? data['title'] : <any>null;
                this.inn = data['inn'] !== undefined ? data['inn'] : <any>null;
                this.kpp = data['kpp'] !== undefined ? data['kpp'] : <any>null;
            }
            this.email = data['email'] !== undefined ? data['email'] : <any>null;
            this.phone = data['phone'] !== undefined ? data['phone'] : <any>null;
        }
    }

    toCompany() {
        const res = new Company();
        res.title = this.title;
        res.inn = this.inn;
        res.kpp = this.kpp;
        res.phone = this.phone;
        res.email = this.email;
        return res;
    }
    toPerson() {
        const res = new Person();

        console.log(this.birthDate);

        res.firstName = this.firstName;
        res.surname = this.surname;
        res.middleName = this.middleName;
        res.birthDate = this.birthDate;
        res.phone = this.phone;
        res.email = this.email;
        return res;
    }
}
