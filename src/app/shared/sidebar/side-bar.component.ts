import { Component, Input } from '@angular/core';
import { Shop } from '../../services/API/models/city-shop.model';
import { Config } from '../../services/web.config';
import { CrossPageDataService } from '../../services/cross-page-data.service';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent {
  constructor(public cpdService: CrossPageDataService) { }
}
