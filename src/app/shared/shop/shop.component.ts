import { Component, Input } from '@angular/core';
import { Shop } from '../../services/API/models/city-shop.model';
import { Config } from '../../services/web.config';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent {
  @Input() shop: Shop;

  get title() { return this.shop ? this.shop.title : ''; }
  get address() { return this.shop ? this.shop.address : ''; }
  get image() { return this.shop ? Config.API_BASE_URL + this.shop.picUrl : ''; }
  get url() { return this.shop ? this.shop.siteUrl : ''; }

  navigateAway() { window.location.href = this.url; }
}
