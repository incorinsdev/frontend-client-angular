import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { DropListValue } from '../../GodModeControls/DropList/drop-list-value.model';
import { CrossPageDataService } from '../../services/cross-page-data.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FieldError, GMTextFieldComponent } from '../../GodModeControls/TextField/text-field.component';
import { Copter } from '../../services/API/models/copter.model';
import { GMDateFunctions } from '../../GodModeControls/Shared/date.functions';

@Component({
  selector: 'app-model-block',
  templateUrl: './modelBlock.component.html',
  styleUrls: ['./modelBlock.component.scss']
})
export class ModelBlockComponent implements OnInit {

  @ViewChild('snControl') snControl: GMTextFieldComponent;

  serialNumberErrors: Array<FieldError> = [];
  yearList: Array<DropListValue> = [];
  get brandList() { return this.cpdService.resBrands.brands; }

  copter = new Copter();

  private _selectedBrand: DropListValue;
  get selectedBrand(): DropListValue { return this._selectedBrand; }
  set selectedBrand(val: DropListValue) {
    if (this._selectedBrand !== val) {
      this._selectedBrand = val;
    }
  }

  _selectedModel: DropListValue;
  get selectedModel() { return this._selectedModel; }
  set selectedModel(val: DropListValue) {
    if (this._selectedModel !== val) {
      this._selectedModel = val;
      this.emitValue();
    }
  }

  _year: DropListValue;
  get year() { return this._year; }
  set year(val: DropListValue) {
    if (this._year !== val) {
      this._year = val;
      this.emitValue();
    }
  }

  formGroup = new FormGroup({
    serialNumberControl: new FormControl('', [Validators.required]),
    modelControl: new FormControl('', [Validators.required])
  });

  @Output() validSatatusChanged: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() copterModelChanges: EventEmitter<Copter> = new EventEmitter<Copter>();


  constructor(public cpdService: CrossPageDataService) {
    this.serialNumberErrors = [];
    this.serialNumberErrors.push(new FieldError('required', 'Заполните поле'));
    const tYear = GMDateFunctions.today().getFullYear();
    this.yearList.push(new DropListValue(tYear - 2, tYear - 2 + ''));
    this.yearList.push(new DropListValue(tYear - 1, tYear - 1 + ''));
    this.yearList.push(new DropListValue(tYear, tYear + ''));
    this._year = this.yearList[2];

    this.formGroup.statusChanges.subscribe(x => { this.validSatatusChanged.emit(this.formGroup.valid); });
    this.formGroup.valueChanges.subscribe(x => { this.emitValue(); });
  }

  private takeCareOfBrands() {
    if (this.cpdService.resBrands.isLoaded) {
      this.selectedBrand = this.cpdService.resBrands.brands[0];
    } else {
      this.cpdService.resBrands.loadComplete.subscribe(success => {
        this.selectedBrand = this.cpdService.resBrands.brands[0];
      });
    }
  }

  ngOnInit(): void {
    this.takeCareOfBrands();
    this._year = this.yearList[2];
  }

  emitValue() {
    const res = new Copter();
    res.serialNumber = this.formGroup.controls.serialNumberControl.value;
    res.modelTitle = this.formGroup.controls.modelControl.value;
    res.manufacturedYear = this.year.value;
    res.brandId = this.selectedBrand.value.id;
    this.copterModelChanges.emit(res);
  }

  pushToCheckErrors() {
    this.snControl.pushToCheckErrors();
  }

}
