import { Component, OnInit, Input } from '@angular/core';
import { CrossPageDataService } from '../../services/cross-page-data.service';
import { DropListValue } from '../../GodModeControls/DropList/drop-list-value.model';
import { City } from 'src/app/services/API/models/city-shop.model';

@Component({
  selector: 'app-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.scss']
})
export class CitiesComponent implements OnInit {

  @Input() type = 'repair';
  _selectedCity = null;
  get selectedCity() { return this._selectedCity; }
  set selectedCity(val: City) {
    if (this._selectedCity !== val) {
      this._selectedCity = val;
    }
  }

  get isLoading() {
    return this.type === 'repair' ? !this.cpdService.repairCities.isLoaded : !this.cpdService.sellerCities.isLoaded;
  }

  get cities() {
    return this.type === 'repair' ? this.cpdService.repairCities.cities : this.cpdService.sellerCities.cities;
  }

  get shops() {
    return this.selectedCity ? this.selectedCity.shops : [];
  }

  _selectedCityInList: DropListValue;
  get selectedCityInList() { return this._selectedCityInList; }
  set selectedCityInList(val: DropListValue) {
    if (this._selectedCityInList !== val) {
      this._selectedCityInList = val;
      this.selectedCity = this.cities.find(city => city.name === val.value);
    }
  }

  get cityList(): Array<DropListValue> {
    return this.cities ? this.cities.map((x) => new DropListValue(x.name, x.name)) : [];
  }

  constructor(public cpdService: CrossPageDataService) { }

  ngOnInit() {
    const data = this.type === 'repair' ? this.cpdService.repairCities : this.cpdService.sellerCities;
    if (data.isLoaded && this.cities.length > 0) {
      this.selectedCity = this.cities[0];
      this.selectedCityInList = new DropListValue(this.cities[0].name, this.cities[0].name);
    } else {
      data.loadComplete.subscribe(x => {
        if (data.isLoaded && this.cities.length > 0) {
          this.selectedCity = this.cities[0];
          this.selectedCityInList = new DropListValue(this.cities[0].name, this.cities[0].name);
        }
      });
    }
  }
}
