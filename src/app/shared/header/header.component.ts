import { Component } from '@angular/core';
import { GMHeaderComponent } from '../../GodModeControls/Header/header.component';
import { CrossPageDataService } from '../../services/cross-page-data.service';
import { Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent extends GMHeaderComponent {
  constructor(public cpdService: CrossPageDataService, router: Router) {
    super(router);
  }

  toggleSidebar () {
    this.cpdService.toggleSidebar();
  }
}
