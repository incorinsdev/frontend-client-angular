import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-payment-status',
  templateUrl: './payment-status.component.html',
  styleUrls: ['./payment-status.component.scss']
})
export class PaymentStatusComponent {
  @Input() type = false;

  constructor(private router: Router) {}

  redirectToHome() {
    this.router.navigate(['/']);
  }

  redirectToOrder() {
    this.router.navigate(['/liability-insurance']);
  }
}
