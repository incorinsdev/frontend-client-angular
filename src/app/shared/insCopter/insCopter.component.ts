import { Component, Input } from '@angular/core';
import { InsModel } from '../../services/API/models/insModel.model';
import { GMStringFormatter } from '../../GodModeControls/Shared/string-formatter.class';
import { Config } from '../../services/web.config';
import { GMModelWindowService } from '../../GodModeControls/ModalWindow/modal.service';
import { InsurenceRequestWindowComponent } from '../../windows/insurence-request-window/insurence-request-window.component';
import { CrossPageDataService } from '../../services/cross-page-data.service';

@Component({
  selector: 'app-ins-copter',
  templateUrl: './insCopter.component.html',
  styleUrls: ['./insCopter.component.scss']
})
export class InsCopterComponent {
  @Input() model = new InsModel();

  constructor(private modalService: GMModelWindowService, private cpd: CrossPageDataService) { }

  get title() { return this.model.brandName + ' ' + this.model.name; }
  get price() { return GMStringFormatter.addSpacesBetweenRanks(this.model.price, 0) + ' руб.'; }
  get url() { return Config.API_BASE_URL + this.model.picturePath; }
  openWindow() {
    const firstWindow = this.modalService.openWindow(InsurenceRequestWindowComponent);
    firstWindow.modelId = this.model.id;
    firstWindow.smService = this.cpd;
  }
}
